package net.evolin.discord;

import net.dv8tion.jda.api.*;
import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.events.message.priv.PrivateMessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.common.utils.SystemUtils;
import net.evolin.discord.commands.CommandHandler;
import net.evolin.discord.sockets.Server;
import org.jline.reader.EndOfFileException;
import org.jline.reader.LineReader;
import org.jline.reader.LineReaderBuilder;
import org.jline.reader.UserInterruptException;
import org.jline.terminal.TerminalBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;

import javax.annotation.Nonnull;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

@SuppressWarnings({"FieldCanBeLocal", "ResultOfMethodCallIgnored"})
public class Discord extends ListenerAdapter {

    private JDA jda;
    private JDABuilder jdaBuilder;
    private String inputFormat = "master@ ~ ";
    private boolean running = true;
    private CommandHandler commandHandler;
    private ConfigHandler configHandler;
    private Server server;
    public static final Logger logger = LoggerFactory.getLogger(Discord.class);
    public LanguageHandler languageHandler;
    public static LineReader lineReader;

    private String languagFileName = "messages.yaml";
    private String configFileName = "config.yaml";

    public static void main(String[] args) {
        // start everything
        new Discord().start();

    }

    /**
     * initialize all configs, creates dirs and copys them if they do not exist
     */
    public void init() {
        String path;
        try {
            // good to do first, because we can print super fancy error logs
            // builds the terminal
            TerminalBuilder builder = TerminalBuilder.builder();
            // builds the linereader (JLine 3)
            lineReader = LineReaderBuilder.builder().terminal(builder.build()).build();

            // get path where the jar is
            path = SystemUtils.getProgramPath(Discord.class);
            // get default system seperator (\ or /)
            String fileSeparator = System.getProperty("file.separator");
            // add the new dir
            String newDirPath = path + fileSeparator + "config" + fileSeparator;
            File newDir = new File(newDirPath);
            // if dir does not exists create it
            if (!newDir.exists())
                newDir.mkdirs();
            // sets and checks if the config file exists - hardcoding messages because yup.
            File languageFile = new File(newDirPath + this.languagFileName);
            if (!languageFile.exists()) {
                Files.copy(Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(languagFileName)), Paths.get(newDirPath + languagFileName));
                logger.error("No languageFile was found. We've created one for you. Feel free to change the default language if needet.");
            }
            // sets and checks if the config file exists - hardcoding messages because yup.
            File configFile = new File(newDirPath + this.configFileName);
            if (!configFile.exists()) {
                Files.copy(Objects.requireNonNull(this.getClass().getClassLoader().getResourceAsStream(configFileName)), Paths.get(newDirPath + this.configFileName));
                logger.error("No configFile was found. We've created one for you. Please change the Port and the Bot token.");
                stop();
            }

            // loads the files into handlers
            Yaml yaml = new Yaml();
            configHandler = yaml.loadAs(new FileInputStream(configFile), ConfigHandler.class);
            languageHandler = yaml.loadAs(new FileInputStream(languageFile), LanguageHandler.class);

            // superbrain check - hardcoding messages because yup.
            if (configHandler.get("discordServer", "PORT", "PORT").equalsIgnoreCase("PORT") || configHandler.get("discordServer", "TOKEN", "TOKEN").equalsIgnoreCase("TOKEN")) {
                logger.error("Please change the PORT and TOKEN in your configuration. You can find the configuration in the `config` folder.");
                stop();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    /**
     * starts the discord bot
     */
    private void start() {
        init();
        // initializing the commandHandler
        this.commandHandler = new CommandHandler(this);
        // initializing jdaBuilder
        this.jdaBuilder = new JDABuilder(AccountType.BOT);
        try {
            // probably have to change the token
            this.jdaBuilder.setToken(this.configHandler.get("discordServer", "TOKEN", "TOKEN"));
            // reconnect on disconnect
            this.jdaBuilder.setAutoReconnect(true);
            // Online Status
            this.jdaBuilder.setStatus(OnlineStatus.ONLINE);
            // Adds a event listener
            this.jdaBuilder.addEventListeners(this);
            // builds the JDA - aka. Discord Bot
            this.jda = this.jdaBuilder.build();
            // start a new Socket server
            this.server = new Server(Integer.parseInt(this.configHandler.get("discordServer", "PORT", "PORT")), this);
            // while running loop for the terminal
            while (running) {

                try {
                    // try to read the input
                    String input = lineReader.readLine(this.inputFormat);

                    // split the input for args
                    String[] inputSlices = input.split(" ");
                    // commandHandler
                    this.commandHandler.handle(inputSlices);
                } catch (UserInterruptException | EndOfFileException e) {
                    stop();
                }
            }

        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            this.jdaBuilder.setStatus(OnlineStatus.OFFLINE);
        }
    }

    /**
     * stops the discord bot
     */
    public void stop() {
        // breaking the command input loop
        this.running = false;
        // initializing the shutodwn
        if (this.jda != null)
            this.jda.shutdown();
        // shutdown the socket thread - will throw a error but will be ignored anyways because of the System.exit(0)
        if (this.server != null)
            try {
                this.server.stop();
            } catch (IOException e) {
                e.printStackTrace();
            }
        // dont touch, waiting for the Bot to fully shutdown
        if (this.jda != null)
            while (true)
                if (this.jda.getStatus() == JDA.Status.SHUTDOWN)
                    break;
        // closing the program, more or less roughly and ugly
        /* TODO: find a better solution for the rough closing */
        System.exit(0);
    }

    /**
     * MessageReceivedEvent gets triggered on any message (private, public)
     *
     * @param event MessageReceivedEvent
     */
    @Override
    public void onMessageReceived(@Nonnull MessageReceivedEvent event) {
        if (event.isFromGuild()) {
            Message msg = event.getMessage();
            if (msg.getContentRaw().equals("!ping")) {
                MessageChannel channel = event.getChannel();
                long time = System.currentTimeMillis();
                channel.sendMessage("Pong!") /* => RestAction<Message> */
                        .queue(response /* => Message */ -> response.editMessageFormat("Pong: %d ms", System.currentTimeMillis() - time).queue());
                String userName = "b0ykoe";
                event.getGuild().getMemberByTag("b0ykoe#0001");
                String avatar = event.getGuild().getMembersByName(userName, true).get(0).getUser().getAvatarUrl();
                EmbedBuilder embedBuilder = new EmbedBuilder();
                embedBuilder.setAuthor("b0ykoe");
                embedBuilder.setImage(avatar);
                embedBuilder.setThumbnail(avatar);
                embedBuilder.addField("test", "test", true);
                embedBuilder.setFooter("Request made @ " + event.getGuild().getIconUrl());
                channel.sendMessage(embedBuilder.build()).queue();
            }
        }
    }

    /**
     * PrivateMessageReceivedEvent gets triggered on a private message
     *
     * @param event PrivateMessageReceivedEvent
     */
    @Override
    public void onPrivateMessageReceived(@Nonnull PrivateMessageReceivedEvent event) {
        System.out.println("Received a Private Message: \n\t" + event.getMessage().getContentDisplay());
    }

    /**
     * getter for the current logger
     *
     * @return Logger - returns the initialized logger
     */
    public Logger getLogger() {
        return logger;
    }
}
