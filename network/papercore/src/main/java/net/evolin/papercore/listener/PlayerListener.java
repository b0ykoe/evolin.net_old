package net.evolin.papercore.listener;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.permission.PermissionUpdateGroupEvent;
import de.dytanic.cloudnet.driver.event.events.permission.PermissionUpdateUserEvent;
import de.dytanic.cloudnet.driver.permission.IPermissionUser;
import de.dytanic.cloudnet.ext.cloudperms.CloudPermissionsManagement;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

public class PlayerListener implements Listener {

    private String format;
    private final JavaPlugin plugin;
    private final PaperCore paperCore;

    /**
     *
     * @param paperCore paperCore instance
     */
    public PlayerListener(PaperCore paperCore) {
        this.plugin = paperCore;
        this.paperCore = paperCore;
        this.format = plugin.getConfig().getString("format");
    }

    /**
     * adds a playerObject to the HashMap
     * @param event PlayerJoinEvent
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void handle(PlayerJoinEvent event) {
        PlayerObject playerObject = new PlayerObject(event.getPlayer(), this.paperCore);
        this.paperCore.addUuidPlayerObject(event.getPlayer().getUniqueId(), playerObject);
        String visibility = playerObject.getProperty("currentVisibility");
        if(visibility != null) {
            playerObject.setVisibility(PlayerObject.VISIBILITY.valueOf(visibility));
            playerObject.change_visibility();
        }

        Bukkit.getScheduler().runTask(this.plugin, playerObject::updateNameTags);
        //Bukkit.getScheduler().runTask(this.plugin, () -> BukkitCloudNetCloudPermissionsPlugin.getInstance().updateNameTags(event.getPlayer()));


        for(Player onlinePlayer : Bukkit.getOnlinePlayers())
        {
            if(onlinePlayer.getUniqueId() != event.getPlayer().getUniqueId())
                this.paperCore.getUuidPlayerObject(onlinePlayer.getUniqueId()).change_visibility(event.getPlayer());
        }

        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateScoreboard", "ALL", new JsonDocument());

    }

    /**
     * removes the playerObject from the HashMap
     * @param event PlayerQuitEvent
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(PlayerQuitEvent event) {
        this.paperCore.removeUuidPlayerObject(event.getPlayer().getUniqueId());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateScoreboard", "ALL", new JsonDocument());
    }

    /**
     * updates the nameTag on user permission update
     * @param event PermissionUpdateUserEvent
     */
    @EventListener
    public void handle(PermissionUpdateUserEvent event) {
        Bukkit.getScheduler().runTask(this.plugin, () -> {

            Bukkit.getOnlinePlayers().stream()
                    .filter(player -> player.getUniqueId().equals(event.getPermissionUser().getUniqueId()))
                    .findFirst()
                    .ifPresent(value -> {
                        this.paperCore.getUuidPlayerObject(value.getUniqueId()).updateCloudNetVariables();
                        this.paperCore.getUuidPlayerObject(value.getUniqueId()).updateNameTags();
                        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateScoreboard", "null", new JsonDocument().append("uuid", event.getPermissionUser().getUniqueId()));
                    });

        });
    }

    /**
     * updates the nameTag on group permission update
     * @param event PermissionUpdateGroupEvent
     */
    @EventListener
    public void handle(PermissionUpdateGroupEvent event) {
        Bukkit.getScheduler().runTask(this.plugin, () -> {

            Bukkit.getOnlinePlayers().forEach(player -> {
                PlayerObject playerObject = this.paperCore.getUuidPlayerObject(player.getUniqueId());
                IPermissionUser permissionUser = CloudPermissionsManagement.getInstance().getUser(player.getUniqueId());
                playerObject.updateCloudNetVariables();

                if (permissionUser != null && permissionUser.inGroup(event.getPermissionGroup().getName())) {
                    this.paperCore.getUuidPlayerObject(player.getUniqueId()).updateNameTags();
                    CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateScoreboard", "ALL", new JsonDocument());
                    //BukkitCloudNetCloudPermissionsPlugin.getInstance().updateNameTags(player);
                }
            });

        });
    }

    /**
     * enforced the CloudNet syntax
     * @param event AsyncPlayerChatEvent
     */
    @EventHandler(priority = EventPriority.HIGH)
    public void handleChat(AsyncPlayerChatEvent event) {
    /*
    TODO :: replace with paperConfig
     */
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(event.getPlayer().getUniqueId());
        String message = playerObject.replaceChatMessage(event.getMessage(), this.format);
        if(message == null || message.isEmpty()) {
            event.setCancelled(true);
            return;
        }

        event.setFormat(message);

    }


}
