package net.evolin.waterfallcore.commands.eco;

import net.evolin.waterfallcore.WaterfallCore;
import net.evolin.waterfallcore.commands.SubCommand;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.List;

public class EcoHelp extends SubCommand{

    private List<SubCommand> subCommands;

    public EcoHelp(WaterfallCore waterfallCore, List<SubCommand> subCommands) {
        super(waterfallCore, "help", "help", "Zeige diese Hilfe hier.");
        this.getAliases().add("hilfe");
        this.subCommands = subCommands;
    }

    @Override
    public void execute(ProxiedPlayer player, String[] args) {
        this.subCommands.forEach(subCommand -> {
            if(subCommand.hasPermission(player))
                player.sendMessage(new TextComponent("§a/eco " + subCommand.getSyntax() + "§7 | " + subCommand.getDescription()));
        });
    }


}
