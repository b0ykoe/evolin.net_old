package net.evolin.module.commands;

import de.dytanic.cloudnet.command.Command;
import de.dytanic.cloudnet.command.ICommandSender;
import de.dytanic.cloudnet.common.Properties;
import net.evolin.module.Module;
import net.evolin.module.sockets.Client;

public class ModuleCommand extends Command {

    private Module module;

    /**
     * Command for CloudNet
     * @param module passing trough our module to access public objects
     */
    public ModuleCommand(Module module) {
        super("evolin", "evo");

        this.module = module;
        this.permission = "evolin.command.module";
        this.prefix = "EvolinNet";
        this.description = this.module.languageHandler.get("module-description", "Module for the Evolin plugin pack.");
    }

    @Override
    public void execute(ICommandSender sender, String command, String[] args, String commandLine, Properties properties) {
        // check if there are arguments and if null send help
        if (args.length == 0) {
            sendHelp(sender);
            return;
        }

        // if there are arguments check for the first one
        // check if the first agument is reload
        if (args[0].equalsIgnoreCase("reload")) {
            // if is reload then load (overrides current config) and sends to all services
            this.module.loadConfig();
            this.module.sendConfig();

            // send a message to the sender to confirm that the config was successfully reload
            sender.sendMessage(this.module.languageHandler.get("module-command-reload-successfully", "The languagefile was reloaded and send to all servers."));
        }

        if (args[0].equalsIgnoreCase("discord")) {

            // if there was no arg applied send help
            if(args.length == 1) {
                sendHelp(sender);
                return;
            }

            // switches trough the args
            switch (args[1]) {
                case "command":
                    // check if the client has been initialized if not print out
                    if (module.client == null) {
                        String status = "Not Initialized use `evolin discord start`";
                        // send a message to the sender
                        sender.sendMessage(this.module.languageHandler.get("module-command-discord-status-successfully", "DiscordBot Status: %status%").replace("%status%", status));
                        break;
                    }
                    StringBuilder stringBuilder = new StringBuilder();
                    // builds the command string
                    for(int i = 2; i < args.length; i++) {
                        // checks if first entry, if not add space upfront
                        if(i != 2)
                            stringBuilder.append(" ");
                        stringBuilder.append(args[i]);
                    }
                    // sends a packet with the string from the stringBuilder
                    module.client.sendCommand(stringBuilder.toString());
                    // send a message to the sender
                    sender.sendMessage(this.module.languageHandler.get("module-command-discord-command-successfully", "The command %command% was send successfully to the DiscordBot.").replace("%command", args[2]));
                    break;
                case "status":
                    String status;
                    // check if the client has been initialized if not print out
                    if(module.client == null)
                        status = "Not Initialized use `evolin discord start`";
                    else
                        // checks if the client is connected
                        if(module.client.isConnected())
                            status = "Connected.";
                        // if set status to Disconnected
                        else
                            status = "Disconnected";
                    // send a message to the sender
                    sender.sendMessage(this.module.languageHandler.get("module-command-discord-status-successfully", "DiscordBot Status: %status%").replace("%status%", status));
                    break;
                case "start":
                    // check if the client has been initialized
                    if(module.client == null) {
                        // send a message to the sender
                        sender.sendMessage(this.module.languageHandler.get("module-command-discord-start-successfully", "The Discordbot will shortly connect."));
                        // if not initialize it
                        module.client = new Client(module.configHandler.get("discordClient", "HOST", "HOST"), Integer.parseInt(module.configHandler.get("discordClient", "PORT", "PORT")), module.configHandler.get("discordClient", "NAME", "NAME") + System.currentTimeMillis());
                        break;
                    }
                    // checks if the client is already connected
                    if(!module.client.isConnected()) {
                        // send a message to the sender
                        sender.sendMessage(this.module.languageHandler.get("module-command-discord-start-successfully", "The Discordbot will shortly connect."));
                        // start the client
                        module.client.start();
                        break;
                    }
                    // send a message to the sender
                    sender.sendMessage(this.module.languageHandler.get("module-command-discord-start-already-connected-successfully", "The DiscordBot is already connected"));
                    break;
                default:
                    // send Help because command wasn't found
                    sendHelp(sender);
                    break;
            }

        }
    }

    public void sendHelp(ICommandSender sender) {
        sender.sendMessage(
                "evolin reload",
                "evolin discord command <command>",
                "evolin discord status",
                "evolin discord start"
                );
    }
}
