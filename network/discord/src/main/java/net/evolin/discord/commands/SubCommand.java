package net.evolin.discord.commands;

import net.evolin.discord.Discord;

import java.util.ArrayList;
import java.util.List;

public abstract class SubCommand {

    protected Discord discord;
    private String name, syntax, description, permission;
    private List<String> aliases;

    /**
     * super for all commands
     *
     * @param discord     discord instance
     * @param name        string - name of the command
     * @param syntax      string - syntax used (ex <arg1> <arg2>)
     * @param description string - discription of the command
     */
    public SubCommand(Discord discord, String name, String syntax, String description) {
        this.discord = discord;
        this.name = name;
        this.syntax = syntax;
        this.description = description;
        this.aliases = new ArrayList<>();
    }

    /**
     * metho on executing
     *
     * @param args stringlist of arguments
     */
    public abstract void execute(String... args);

    /**
     * getter for commandname
     *
     * @return string - name of the command
     */
    @SuppressWarnings("WeakerAccess")
    public String getName() {
        return this.name;
    }

    /**
     * getter for syntax
     *
     * @return string - syntax of command
     */
    public String getSyntax() {
        return this.syntax;
    }

    /**
     * getter for aliases of the command (ex. help, h)
     *
     * @return list<String> -  all aliases
     */
    @SuppressWarnings("WeakerAccess")
    public List<String> getAliases() {
        return this.aliases;
    }

    /**
     * getter for aliases of the command (ex. help, h) as string
     *
     * @return string - all aliases
     */
    public String getAliasesString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.aliases.size(); i++) {
            if (i != 0)
                stringBuilder.append(", ");
            stringBuilder.append(this.aliases.get(i));
        }
        return stringBuilder.toString();
    }

    /**
     * getter for the command description
     *
     * @return string - description of the command
     */
    public String getDescription() {
        return this.description;
    }

    /**
     * getter for the command permission
     *
     * @return string - permission used for the command
     */
    public String getPermission() {
        return this.permission;
    }

}
