package net.evolin.module.listener;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import net.evolin.module.Module;

import java.util.HashMap;
import java.util.UUID;

public class ConfigListener{

    private Module module;
    public ConfigListener(Module module) {
        this.module = module;
    }

    /**
     * This function checks if the config is requested and sends it afterwards back to the service
     * @param event ChannelMessageReceiveEvent triggered when a channel message is send
     */
    @EventListener
    public void handleChannelMessage(ChannelMessageReceiveEvent event) {
        // check for requestConfig channel
        if (event.getChannel().equalsIgnoreCase("requestConfig")) {
            // get the serviceInfoSnapShot from the service
            ServiceInfoSnapshot serviceInfoSnapshot = CloudNetDriver.getInstance().getCloudServiceProvider().getCloudService(UUID.fromString(event.getMessage()));
            // send config and auth with the UUID from the service
            this.module.sendConfig(serviceInfoSnapshot);
        }

        if (event.getChannel().equalsIgnoreCase("missingConfig")) {
            HashMap<String, String> value = this.module.fileHandler.config.containsKey(event.getData().getString("authority")) ? this.module.fileHandler.config.get(event.getData().getString("authority")) : new HashMap<>();
            value.put(event.getData().getString("key"), event.getData().getString("value"));
            this.module.fileHandler.config.put(event.getData().getString("authority"), value);
        }

        if (event.getChannel().equalsIgnoreCase("missingLanguage")) {
            String language = event.getData().getString("language");
            String authority = event.getData().getString("authority");
            String key = event.getData().getString("key");
            String value =event.getData().getString("value");

            // if language does not exist create
            if(!this.module.fileHandler.messages.containsKey(language))
                this.module.fileHandler.messages.put(language, new HashMap<>());

            // if authority does not exist create
            if(!this.module.fileHandler.messages.get(language).containsKey(authority))
                this.module.fileHandler.messages.get(language).put(authority, new HashMap<>());

            // if key does not exist create
            if(!this.module.fileHandler.messages.get(language).get(authority).containsKey(key))
                this.module.fileHandler.messages.get(language).get(authority).put(key, value);

        }
    }
}
