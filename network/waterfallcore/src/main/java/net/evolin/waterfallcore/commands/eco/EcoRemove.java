package net.evolin.waterfallcore.commands.eco;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.common.utils.SystemUtils;
import net.evolin.waterfallcore.WaterfallCore;
import net.evolin.waterfallcore.commands.SubCommand;
import net.evolin.common.objects.EcoObject;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class EcoRemove extends SubCommand {
    public EcoRemove(WaterfallCore waterfallCore) {
        super(waterfallCore, "remove", "system.eco.remove","remove [name] <amount>", "Nimmt eine Menge an Money weg");
    }

    @Override
    public void execute(ProxiedPlayer player, String[] args) {
        ProxiedPlayer receiver = null;
        ICloudPlayer iCloudPlayer = null;
        int amount = 0;

        if(!this.hasPermission(player))
            return;

        if(args.length == 0)
            return;

        if(args.length == 1) {
            receiver = player;
            amount = SystemUtils.parseWithDefault(args[0], 0);
        }

        if(args.length == 2) {
            receiver = ProxyServer.getInstance().getPlayer(args[0]);
            amount = SystemUtils.parseWithDefault(args[1], 0);
        }

        if(receiver == null) {
            player.sendMessage("Der Spieler %s ist nicht online".replace("%s", args[0]));
            return;
        }

        iCloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(receiver.getUniqueId());

        EcoObject.removeCoins(iCloudPlayer, amount);
        if(!iCloudPlayer.getName().equals(player.getName())) {
            player.sendMessage("Du hast die Coins von dem Spieler %s auf %i verringert".replace("%s", iCloudPlayer.getName()).replace("%i", String.valueOf(EcoObject.getCoins(iCloudPlayer))));
            BridgePlayerManager.getInstance().proxySendPlayerMessage(iCloudPlayer, "Deine Eco wurde auf %i verringert".replace("%i", String.valueOf(EcoObject.getCoins(iCloudPlayer))));
        } else
            player.sendMessage("Du hast deine Coins auf %i verringert".replace("%i", String.valueOf(EcoObject.getCoins(iCloudPlayer))));
    }
}
