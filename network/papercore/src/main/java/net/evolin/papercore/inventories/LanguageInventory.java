package net.evolin.papercore.inventories;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.entity.Player;

public class LanguageInventory implements InventoryProvider {

    private PaperCore paperCore;
    private LanguageHandler languageHandler;
    public LanguageInventory(PaperCore paperCore) {
        this.paperCore = paperCore;
        this.languageHandler = this.paperCore.getLanguageHandler();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(player.getUniqueId());
        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.SettingsMenuSub(contents, playerObject, this.languageHandler);
        InventoryUtils.ProfileMenu(contents, playerObject, this.languageHandler);


        contents.set(1, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemLanguageGerman").build(), e -> {
            playerObject.addProperty("defaultLanguage", "german");
            playerObject.sendMessageToPlayer(playerObject.getDefaultLang(this.languageHandler, "default-language-german", "Deine Standardsprache wurde auf §cDeutsch §rumgestellt."));
            playerObject.openDefaultInventory("inventoryLanguage");
        }));

        contents.set(1, 6, ClickableItem.of(playerObject.getDefaultItemObject("itemLanguageEnglish").build(), e -> {
            playerObject.addProperty("defaultLanguage", "english");
            playerObject.sendMessageToPlayer(playerObject.getDefaultLang(this.languageHandler, "default-language-english", "Your default language has been set to §cEnglish§r."));
            playerObject.openDefaultInventory("inventoryLanguage");
        }));

    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
}
