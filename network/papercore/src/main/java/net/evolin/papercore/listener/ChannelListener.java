package net.evolin.papercore.listener;

import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;

import java.util.UUID;

public class ChannelListener {
    private final PaperCore paperCore;

    /**
     * basic Listener constructor
     * @param paperCore passes trough the PaperCore Plugin to access objects
     */
    public ChannelListener(PaperCore paperCore) {
        this.paperCore = paperCore;
    }

    /**
     * waits for incomming channel messages -> custom channeling
     * @param event ChannelMessageReceiveEvent from CloudNet
     */
    @EventListener
    public void handleChannelMessage(ChannelMessageReceiveEvent event) {
        if (event.getChannel().equalsIgnoreCase("updateScoreboard")) {
            if(event.getMessage().equalsIgnoreCase("ALL")) {
                this.paperCore.getUuidPlayerMap().forEach((k, v) -> {
                    if (v.getScoreboardHandler() != null)
                        v.getScoreboardHandler().update();
                });
            } else {
                PlayerObject playerObject = this.paperCore.getUuidPlayerObject(UUID.fromString(event.getData().getString("uuid")));
                if (playerObject.getScoreboardHandler() != null)
                    playerObject.getScoreboardHandler().update();
            }
        }
    }
}
