package net.evolin.lobby.inventories;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.lobby.inventories.gadgets.ParticleInventory;
import net.evolin.lobby.inventories.gadgets.ShoeInventory;
import net.evolin.papercore.objects.ItemObject;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class LobbyInventory {

    public static void setDefaultHotbar(Player player, PlayerObject playerObject, LanguageHandler languageHandler) {
        Inventory inventory = player.getInventory();
        inventory.clear();

        inventory.setItem(1, playerObject.getDefaultItemObject("itemGadgets").build());
        inventory.setItem(3, playerObject.getDefaultItemObject("itemHide").build());
        inventory.setItem(5, playerObject.getDefaultItemObject("itemTeleporter").build());

        ItemStack head = new ItemObject(playerObject.getPlayerHead()).setDisplayName(playerObject.getDefaultLang(languageHandler, "itemProfileTitle", "§cProfile")).build().clone();
        inventory.setItem(7, head);
    }

    public static void loadGadgetProperties(Player player, PlayerObject playerObject) {
        ICloudPlayer cloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());

        if(cloudPlayer.getProperties().contains("currentBalloon") && !(cloudPlayer.getProperties().getString("currentBalloon").equalsIgnoreCase("none")))
            playerObject.setBalloon(playerObject.getDefaultItemObject(cloudPlayer.getProperties().getString("currentBalloon")).build(), cloudPlayer.getProperties().getString("currentBalloon"));
        if(cloudPlayer.getProperties().contains("currentShoes") && !(cloudPlayer.getProperties().getString("currentShoes").equalsIgnoreCase("none")))
            ShoeInventory.setShoes(playerObject, player, cloudPlayer.getProperties().getString("currentShoes"));
        if(cloudPlayer.getProperties().contains("currentParticlesItem") && !(cloudPlayer.getProperties().getString("currentParticlesItem").equalsIgnoreCase("none")) &&
                cloudPlayer.getProperties().contains("currentParticlesParticles") && !(cloudPlayer.getProperties().getString("currentParticlesParticles").equalsIgnoreCase("none")))
            ParticleInventory.setParticle(playerObject, player, cloudPlayer.getProperties().getString("currentParticlesItem"), Particle.valueOf(cloudPlayer.getProperties().getString("currentParticlesParticles")));

    }
}
