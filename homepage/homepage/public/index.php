<?php

use GuzzleHttp\Cookie\CookieJar;

require __DIR__ . '/../vendor/autoload.php';

header('Content-Type: application/json');

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);

Flight::route('GET /', function(){
    // Use a specific cookie jar
    $jar = new \GuzzleHttp\Cookie\CookieJar;
    // Create a client with a base URI
    $client = new GuzzleHttp\Client(['base_uri' => 'http://94.130.44.236:2812', 'cookies' => $jar]);
    // Send a request to https://foo.com/api/test
    $response = $client->request('GET', '/api/v1/auth', ['auth' => ['b0ykoe', '123456']]);
    print_r($jar);
});

Flight::route('GET /status', function(){

        $jar = CookieJar::fromArray(
            [
                'CloudNet-REST_V1-Session.-290008327' => 'EC0Q8EbCGUx/khwax6vZQzZy6RLxRTGG69K/UbFk0yg=',
            ],
            '94.130.44.236'
        );

        // Create a client with a base URI
        $client = new GuzzleHttp\Client(['cookies' => $jar]);
        // Send a request to https://foo.com/api/test
        $response = $client->request('GET', 'http://94.130.44.236:2812/api/v1/status');
        print_r($response->getBody()->getContents());
});

Flight::start();
