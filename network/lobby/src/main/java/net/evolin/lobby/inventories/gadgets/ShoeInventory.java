package net.evolin.lobby.inventories.gadgets;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.lobby.Lobby;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.gadgets.gadgets.ShoeGadget;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.inventories.InventoryUtils;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ShoeInventory implements InventoryProvider {

    private Lobby lobby;
    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;
    private ItemHandler itemHandler;

    public ShoeInventory(Lobby lobby) {
        this.lobby = lobby;
        this.configHandler = this.lobby.paperCore.getConfigHandler();
        this.languageHandler = this.lobby.paperCore.getLanguageHandler();
        this.itemHandler = this.lobby.paperCore.getItemHandler();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());
        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.GadgetMenue(contents, playerObject);

        ItemStack item = null;
        if (playerObject.getGadgetManager().hasState(GadgetStates.SHOES)) {
            item = ((ShoeGadget) playerObject.getGadgetManager().getState(GadgetStates.SHOES)).getSelectedItem();
        }

        contents.set(1, 2, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemAquaShoes", playerObject), event -> setShoes(playerObject, player, "itemAquaShoes") ));
        contents.set(1, 4, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemGrayShoes", playerObject), event -> setShoes(playerObject, player, "itemGrayShoes") ));
        contents.set(1, 6, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemOrangeShoes", playerObject), event -> setShoes(playerObject, player, "itemOrangeShoes") ));
    }

    public static void setShoes(PlayerObject playerObject, Player player, String item) {
        playerObject.addProperty("currentShoes", item);
        playerObject.getGadgetManager().setState(GadgetStates.SHOES);
        if (playerObject.getGadgetManager().hasState(GadgetStates.SHOES)) {
            ((ShoeGadget) playerObject.getGadgetManager().getState(GadgetStates.SHOES)).setShoes(playerObject.getDefaultItemObject(item).build());
            player.closeInventory();
            player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
        }
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
}
