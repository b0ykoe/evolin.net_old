package net.evolin.common.utils;

import java.io.File;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;

public class SystemUtils {
    /**
     * gets the current programmpath
     *
     * @param programClass class of the main programm
     * @return string - string to the current folder of jarfile
     */
    public static String getProgramPath(Class programClass) {
        URL url = programClass.getProtectionDomain().getCodeSource().getLocation();
        String jarPath = URLDecoder.decode(url.getFile(), StandardCharsets.UTF_8);
        return new File(jarPath).getParentFile().getPath();
    }

    /**
     * parses a string to a integer and adds a default value if not valid
     *
     * @param s          String
     * @param defaultVal Integer - default value
     * @return int - either string as int or default value
     */
    public static int parseWithDefault(String s, int defaultVal) {
        return s.matches("-?\\d+") ? Integer.parseInt(s) : defaultVal;
    }
}
