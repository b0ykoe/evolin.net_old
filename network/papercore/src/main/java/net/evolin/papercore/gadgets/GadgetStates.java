package net.evolin.papercore.gadgets;

import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.inventory.ItemStack;

public abstract class GadgetStates {

    public static final int PARTICLES = 0,
            BALLOONS = 1,
            SHOES = 2;
    protected boolean asyncTasks = false;

    protected ItemStack selectedItem;

    public abstract void start(PaperCore paperCore, PlayerObject playerObject);
    public void run(){}
    public void runAsync(){}
    public abstract void stop();

    @SuppressWarnings("WeakerAccess")
    public boolean hasAsyncTasks(){
        return asyncTasks;
    }

    public ItemStack getSelectedItem() {
        return selectedItem;
    }
}

