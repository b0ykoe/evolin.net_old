package net.evolin.lobby.inventories;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.common.utils.ConfigUtils;
import net.evolin.lobby.Lobby;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.inventories.InventoryUtils;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Location;
import org.bukkit.entity.Player;

public class TeleporterInventory implements InventoryProvider {
    private Lobby lobby;
    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;
    private ItemHandler itemHandler;

    public TeleporterInventory(Lobby lobby) {
        this.lobby = lobby;
        this.configHandler = this.lobby.paperCore.getConfigHandler();
        this.languageHandler = this.lobby.paperCore.getLanguageHandler();
        this.itemHandler = this.lobby.paperCore.getItemHandler();
    }



    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());
        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.CurrentServerItem(contents, playerObject, this.languageHandler);
        InventoryUtils.TeleporterMenue(contents, playerObject);

        Location spawn = ConfigUtils.locationFromString(this.lobby.paperCore.getConfigHandler().get("lobby", "spawnLocation", "lobby_thomas,0.5,101,0.5,-180,0"));
        Location game1 = ConfigUtils.locationFromString(this.lobby.paperCore.getConfigHandler().get("lobby", "spawnGame1", "lobby_thomas,20.5,101,-23.5,180,0"));
        Location game2 = ConfigUtils.locationFromString(this.lobby.paperCore.getConfigHandler().get("lobby", "spawnGame2", "lobby_thomas,-12.5,101,-71.5,90,0"));

        contents.set(1, 4, ClickableItem.of(playerObject.getDefaultItemObject("itemSpawn").build(), e -> { player.teleport(spawn); }));
        contents.set(1, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemGame1").build(), e -> { player.teleport(game1); }));
        contents.set(1, 6, ClickableItem.of(playerObject.getDefaultItemObject("itemGame2").build(), e -> { player.teleport(game2); }));

    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

}
