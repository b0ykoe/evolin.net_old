package net.evolin.discord.commands.commands;

import net.evolin.discord.Discord;
import net.evolin.discord.commands.SubCommand;

public class ExitCommand extends SubCommand {

    /**
     * adds the exit command to the commandhandler
     *
     * @param discord discord instance
     */
    public ExitCommand(Discord discord) {
        super(discord, "exit", "exit", discord.languageHandler.get("exit-command-description", "Shutdowns the Bot."));
        this.getAliases().add("close");
        this.getAliases().add("shutdown");
    }

    /**
     * executes the exit command
     *
     * @param args stringlist of arguments (there none for this command)
     */
    @Override
    public void execute(String... args) {
        this.discord.getLogger().info(discord.languageHandler.get("exit-command-sucessfully", "Bot is shutting down.."));
        this.discord.stop();
    }
}
