package net.evolin.papercore.objects;

import com.destroystokyo.paper.profile.PlayerProfile;
import de.dytanic.cloudnet.driver.permission.IPermissionGroup;
import de.dytanic.cloudnet.driver.permission.IPermissionUser;
import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudOfflinePlayer;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.cloudperms.CloudPermissionsManagement;
import fr.minuskube.inv.SmartInventory;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.common.utils.CloudUtils;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.gadgets.GadgetManager;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.gadgets.gadgets.BalloonGadget;
import net.evolin.papercore.handler.ScoreboardHandler;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scoreboard.Team;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class PlayerObject extends net.evolin.common.objects.PlayerObject<Player> {

    public enum VISIBILITY {
        NONE, VIP, ALL
    }

    private ScoreboardHandler scoreboardHandler;

    public ScoreboardHandler getScoreboardHandler() {
        return scoreboardHandler;
    }

    public void setScoreboardHandler(ScoreboardHandler scoreboardHandler) {
        this.scoreboardHandler = scoreboardHandler;
    }

    private Player player;

    public ICloudPlayer getiCloudPlayer() {
        return BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());
    }
    private String startMoney = "0";
    private ICloudPlayer iCloudPlayer;
    private IPermissionGroup iPermissionGroup;
    private IPermissionUser iPermissionUser;

    private PlayerProfile defaultProfile;
    private PlayerProfile currentProfile;

    private String texture;
    private String signature;
    private HashMap<String, String> defaults = new HashMap<>();
    private PaperCore paperCore;
    private GadgetManager gadgetManager;
    private VISIBILITY visibility = VISIBILITY.ALL;
    public UUID fakeUUID;
    private String suffix;
    private String prefix;
    private String color;
    private String display;
    private String groupName;
    private boolean isNicked = false;

    /**
     * gets the default Player profile
     *
     * @return PlayerProfile
     */
    public PlayerProfile getDefaultProfile() {
        return defaultProfile;
    }

    /**
     * gets the current profile
     *
     * @return PlayerProfile
     */
    public PlayerProfile getCurrentProfile() {
        return currentProfile;
    }

    /**
     * sets the current profile for a player (used when nicked or unnicked)
     *
     * @param currentProfile PlayerProfile
     */
    public void setCurrentProfile(PlayerProfile currentProfile) {
        this.currentProfile = currentProfile;
    }

    /**
     * get if a player is nicked
     *
     * @return true if is nicked
     */
    public boolean isNicked() {
        return isNicked;
    }

    /**
     * set if a player is nicked
     *
     * @param nicked boolean
     */
    public void setNicked(boolean nicked) {
        isNicked = nicked;
    }

    /**
     * returns the suffix from the tablist + chat
     *
     * @return string
     */
    public String getGroupName() {
        return suffix;
    }

    /**
     * sets the groupName (mostly chat) for the name, max 16 length. this includes color tags (§c -> 2)
     *
     * @param groupName string - groupName - ex Owner
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * returns the suffix from the tablist + chat
     *
     * @return string
     */
    public String getDisplay() {
        return suffix;
    }

    /**
     * sets the display (mostly chat) for the name, max 16 length. this includes color tags (§c -> 2)
     *
     * @param display string - display - ex §cAdmin
     */
    public void setDefaults(String display) {
        this.display = display;
    }

    /**
     * returns the suffix from the tablist + chat
     *
     * @return string
     */
    public String getSuffix() {
        return suffix;
    }

    /**
     * sets the suffix (SUFFIX, AFTER) for the name, max 16 length. this includes color tags (§c -> 2)
     *
     * @param suffix string - suffix - ex Admin |
     */
    public void setSuffix(String suffix) {
        this.suffix = suffix;
    }

    /**
     * returns the prefix from the tablist + chat
     *
     * @return string
     */
    public String getPrefix() {
        return prefix;
    }

    /**
     * sets the prefix (PRE, BEVORE) for the name, max 16 length. this includes color tags (§c -> 2)
     *
     * @param prefix string - prefix - ex Admin |
     */
    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    /**
     * returns the color of the tablist
     *
     * @return string color ex. ex. §c or &c
     */
    public String getColor() {
        return color;
    }

    /**
     * sets the color for the team -> Color from the name in tab
     *
     * @param color minecraft color ex. §c or &c
     */
    public void setColor(String color) {
        this.color = color;
    }

    public PlayerObject(Player player, PaperCore paperCore) {
        super(player.getName(), player.getUniqueId());
        this.player = player;
        this.paperCore = paperCore;
        this.iCloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());
        this.iPermissionUser = CloudPermissionsManagement.getInstance().getUser(player.getUniqueId());
        this.iPermissionGroup = CloudPermissionsManagement.getInstance().getHighestPermissionGroup(this.iPermissionUser);
        this.defaultProfile = player.getPlayerProfile();
        this.currentProfile = player.getPlayerProfile();

        this.gadgetManager = new GadgetManager(this.paperCore, this);
        addDefaults();
        checkDefaultProperties();
        this.setLanguage(this.iCloudPlayer.getProperties().contains("defaultLanguage") ? this.iCloudPlayer.getProperties().getString("defaultLanguage") : "english");
    }

    /**
     * getter for permissionGroup from CloudNet
     *
     * @return IPermissionGroup
     */
    public IPermissionGroup getiPermissionGroup() {
        return iPermissionGroup;
    }

    /**
     * setter for iPermissionGroup
     *
     * @param iPermissionGroup iPermissionGroup
     */
    public void setiPermissionGroup(IPermissionGroup iPermissionGroup) {
        this.iPermissionGroup = iPermissionGroup;
    }

    /**
     * getter for permissionUser from CloudNet
     *
     * @return IPermissionUser
     */
    public IPermissionUser getiPermissionUser() {
        return iPermissionUser;
    }

    /**
     * setter for iPermissionUser
     *
     * @param iPermissionUser iPermissionUser
     */
    public void setiPermissionUser(IPermissionUser iPermissionUser) {
        this.iPermissionUser = iPermissionUser;
    }

    /**
     * creates a player object with username and uuid (example for bungee)
     *
     * @param username username from the player
     * @param uuid     uuid from the player
     */
    public PlayerObject(String username, UUID uuid) {
        super(username, uuid);
    }

    /**
     * creates a player object with username, uuid, texture and signature (example paper)
     *
     * @param username  username from the player
     * @param uuid      uuid from the player
     * @param texture   texture from the player
     * @param signature signature from the player
     */
    public PlayerObject(String username, UUID uuid, String texture, String signature) {
        super(username, uuid);
        this.texture = texture;
        this.signature = signature;
    }

    public void updateCloudNetVariables() {
        this.iPermissionUser = CloudPermissionsManagement.getInstance().getUser(player.getUniqueId());
        this.iPermissionGroup = CloudPermissionsManagement.getInstance().getHighestPermissionGroup(this.iPermissionUser);
    }

    /**
     * check if is nicked, if send default group, if not send real group
     *
     * @return IPermissionGroup
     */
    private IPermissionGroup getPermissionGroupDependsNick() {
        return this.isNicked ? CloudPermissionsManagement.getInstance().getDefaultPermissionGroup() : this.iPermissionGroup;
    }

    /**
     * check if is nicked, if send default prefix, if not send real group prefix or custom prefix
     *
     * @return String
     */
    public String getPrefixDependsNick() {
        if (this.isNicked)
            return getPermissionGroupDependsNick().getPrefix();

        return this.prefix == null ? getPermissionGroupDependsNick().getPrefix() : this.prefix;
    }

    /**
     * check if is nicked, if send default suffix, if not send real group suffix or custom suffix
     *
     * @return String
     */
    public String getSuffixDependsNick() {
        if (this.isNicked)
            return getPermissionGroupDependsNick().getColor();

        return this.suffix == null ? getPermissionGroupDependsNick().getSuffix() : this.suffix;
    }

    /**
     * check if is nicked, if send default color, if not send real group color or custom color
     *
     * @return String
     */
    public String getColorDependsNick() {
        if (this.isNicked)
            return getPermissionGroupDependsNick().getColor();

        return this.color == null ? getPermissionGroupDependsNick().getColor() : this.color;
    }

    /**
     * check if is nicked, if send default display, if not send real group display or custom display
     *
     * @return String
     */
    public String getDisplayDependsNick() {
        if (this.isNicked)
            return getPermissionGroupDependsNick().getDisplay();

        return this.display == null ? getPermissionGroupDependsNick().getDisplay() : this.display;
    }

    /**
     * check if is nicked, if send default groupName, if not send real group groupName or custom groupName
     *
     * @return String
     */
    public String getGroupNameDependsNick() {
        if (this.isNicked)
            return getPermissionGroupDependsNick().getName();

        return this.groupName == null ? getPermissionGroupDependsNick().getName() : this.groupName;
    }


    /**
     * updates the nametags after a player joined or a event happend
     */
    public void updateNameTags() {
        IPermissionGroup playerPermissionGroup = getPermissionGroupDependsNick();

        initScoreboard(player);

        Bukkit.getOnlinePlayers().forEach(all -> {
            initScoreboard(all);

            addTeamEntry(player, all, this);

            PlayerObject targetPlayerObject = this.paperCore.getUuidPlayerObject(all.getUniqueId());
            IPermissionGroup targetPermissionGroup = targetPlayerObject.getPermissionGroupDependsNick();

            addTeamEntry(all, player, targetPlayerObject);
        });
    }

    /**
     * adds a teamentry given on the target, "all" player (looping) and the given playerobject for the player
     *
     * @param target       targetplayer who should get the entry
     * @param all          onlineplayer - the player that the entry comes from
     * @param playerObject playerobject - depends on the situation ^look above
     */
    private void addTeamEntry(Player target, Player all, PlayerObject playerObject) {
        IPermissionGroup permissionGroup = playerObject.getPermissionGroupDependsNick();
        String teamName = permissionGroup.getSortId() + permissionGroup.getName();

        if (teamName.length() > 16) {
            teamName = teamName.substring(0, 16);
        }

        Team team = all.getScoreboard().getTeam(teamName);
        if (team == null) {
            team = all.getScoreboard().registerNewTeam(teamName);
        }

        String prefix = playerObject.getPrefixDependsNick();
        String suffix = playerObject.getSuffixDependsNick();
        String color = playerObject.getColorDependsNick();

        try {
            Method method = team.getClass().getDeclaredMethod("setColor", ChatColor.class);
            method.setAccessible(true);

            if (color != null && !color.isEmpty()) {
                ChatColor chatColor = ChatColor.getByChar(color.replaceAll("&", "").replaceAll("§", ""));
                if (chatColor != null) {
                    method.invoke(team, chatColor);
                }
            } else {
                color = ChatColor.getLastColors(prefix.replace('&', '§'));
                if (!color.isEmpty()) {
                    ChatColor chatColor = ChatColor.getByChar(color.replaceAll("&", "").replaceAll("§", ""));
                    if (chatColor != null) {
                        permissionGroup.setColor(color);
                        CloudPermissionsManagement.getInstance().updateGroup(permissionGroup);
                        method.invoke(team, chatColor);
                    }
                }
            }
        } catch (NoSuchMethodException ignored) {
        } catch (IllegalAccessException | InvocationTargetException exception) {
            exception.printStackTrace();
        }

        team.setPrefix(ChatColor.translateAlternateColorCodes('&',
                /*prefix.length() > 16 ?
                        prefix.substring(0, 16) :*/ prefix));

        team.setSuffix(ChatColor.translateAlternateColorCodes('&',
                /*suffix.length() > 16 ?
                        suffix.substring(0, 16) :*/ suffix));

        team.addEntry(target.getName());

        //target.setDisplayName(ChatColor.translateAlternateColorCodes('&', permissionGroup.getDisplay() + target.getName()));
        //target.setPlayerListName(ChatColor.translateAlternateColorCodes('&', permissionGroup.getDisplay() + target.getName()));

        target.setDisplayName(ChatColor.translateAlternateColorCodes('&', prefix + color + playerObject.getPlayer().getName() + suffix));
        target.setPlayerListName(ChatColor.translateAlternateColorCodes('&', prefix + color + playerObject.getPlayer().getName() + suffix));

    }

    /**
     * copyright by cloudnet
     * initializes a scoreboard if none is given
     *
     * @param player bukkit player
     */
    private void initScoreboard(Player player) {
        if (player.getScoreboard().equals(player.getServer().getScoreboardManager().getMainScoreboard())) {
            player.setScoreboard(player.getServer().getScoreboardManager().getNewScoreboard());
        }
    }

    /**
     * replaces the normal chat messages with the format given by the server
     *
     * @param message message from the event event.getmessage()
     * @param format  format given by the server
     * @return returns full format (event.setformat())
     */
    public String replaceChatMessage(String message, String format) {
        IPermissionGroup playerPermissionGroup = getPermissionGroupDependsNick();

        final String prefix = getPrefixDependsNick();
        final String suffix = getSuffixDependsNick();
        final String color = getColorDependsNick();
        final String display = getDisplayDependsNick();

        message = message.replace("%", "%%");
        if (player.hasPermission("cloudnet.chat.color")) {
            message = ChatColor.translateAlternateColorCodes('&', message);
        }

        if (ChatColor.stripColor(message).trim().isEmpty()) {
            return null;
        }

        format = format
                .replace("%name%", player.getName())
                .replace("%uniqueId%", player.getUniqueId().toString());

        if (playerPermissionGroup != null) {
            format = ChatColor.translateAlternateColorCodes('&',
                    format
                            .replace("%group%", playerPermissionGroup.getName())
                            .replace("%display%", display)
                            .replace("%prefix%", prefix)
                            .replace("%suffix%", suffix)
                            .replace("%color%", color)
            );
        } else {
            format = ChatColor.translateAlternateColorCodes('&',
                    format
                            .replace("%group%", "")
                            .replace("%display%", "")
                            .replace("%prefix%", "")
                            .replace("%suffix%", "")
                            .replace("%color%", "")
            );
        }

        return format.replace("%message%", message);
    }

    /**
     * @param item
     */
    public void setBalloon(ItemStack item, String balloonName) {
        this.addProperty("currentBalloon", balloonName);
        this.getGadgetManager().setState(GadgetStates.BALLOONS);
        if (this.getGadgetManager().hasState(GadgetStates.BALLOONS)) {
            BalloonGadget gadget = (BalloonGadget) this.getGadgetManager().getState(GadgetStates.BALLOONS);
            gadget.setItem(item);
            this.player.closeInventory();
            this.player.playSound(this.player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
        }
    }


    /**
     * returns the players gadgetManager
     *
     * @return gadgetManager
     */
    public GadgetManager getGadgetManager() {
        return this.gadgetManager;
    }

    /**
     * gets the current visibility for the player
     *
     * @return enum playerObject#VISIBILITY
     */
    public VISIBILITY getVisibility() {
        return visibility;
    }

    /**
     * sets the current visibility for a player
     *
     * @param visibility enum playerObject#VISIBILITY
     */
    public void setVisibility(VISIBILITY visibility) {
        this.visibility = visibility;
    }

    public void change_visibility() {
        Bukkit.getOnlinePlayers().forEach(this::change_visibility);
    }

    public void change_visibility(Player p) {
        if (p == this.player)
            return;
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(p.getUniqueId());
        switch (visibility) {
            case VIP:
                if (!p.hasPermission(this.paperCore.getConfigHandler().get("CORE", "permsVIP", "network.status.VIP"))) {
                    hidePlayer(playerObject);
                } else {
                    if (canSee(playerObject)) {
                        showPlayer(playerObject);
                    }
                }
                break;
            case NONE:
                hidePlayer(playerObject);
                break;
            case ALL:
                if (canSee(playerObject)) {
                    showPlayer(playerObject);
                }
                break;
            default:
                break;
        }
    }

    public void showPlayer(PlayerObject playerObject) {
        playerObject.player.showPlayer(this.paperCore, this.player);
        this.player.showPlayer(this.paperCore, playerObject.player);
        if (this.gadgetManager.hasState(GadgetStates.BALLOONS)) {
            BalloonGadget balloonGadget = (BalloonGadget) this.getGadgetManager().getState(GadgetStates.BALLOONS);
            if (!this.paperCore.getEntityHider().canSee(playerObject.player, balloonGadget.getSheep())) {
                this.paperCore.getEntityHider().showEntity(playerObject.player, balloonGadget.getSheep());
                balloonGadget.getSheep().setLeashHolder(this.player);
            }
            if (!this.paperCore.getEntityHider().canSee(playerObject.player, balloonGadget.getArmorStand()))
                this.paperCore.getEntityHider().showEntity(playerObject.player, balloonGadget.getArmorStand());
        }
        if (playerObject.gadgetManager.hasState(GadgetStates.BALLOONS)) {
            BalloonGadget balloonGadget = (BalloonGadget) playerObject.getGadgetManager().getState(GadgetStates.BALLOONS);
            if (!this.paperCore.getEntityHider().canSee(this.player, balloonGadget.getSheep())) {
                this.paperCore.getEntityHider().showEntity(this.player, balloonGadget.getSheep());
                balloonGadget.getSheep().setLeashHolder(playerObject.player);
            }
            if (!this.paperCore.getEntityHider().canSee(this.player, balloonGadget.getArmorStand()))
                this.paperCore.getEntityHider().showEntity(this.player, balloonGadget.getArmorStand());
        }
    }

    public void hidePlayer(PlayerObject playerObject) {
        playerObject.player.hidePlayer(this.paperCore, this.player);
        this.player.hidePlayer(this.paperCore, playerObject.player);
        if (getGadgetManager().hasState(GadgetStates.BALLOONS)) {
            BalloonGadget balloonGadget = (BalloonGadget) getGadgetManager().getState(GadgetStates.BALLOONS);
            if (this.paperCore.getEntityHider().canSee(playerObject.player, balloonGadget.getSheep()))
                this.paperCore.getEntityHider().hideEntity(playerObject.player, balloonGadget.getSheep());
            if (this.paperCore.getEntityHider().canSee(playerObject.player, balloonGadget.getArmorStand()))
                this.paperCore.getEntityHider().hideEntity(playerObject.player, balloonGadget.getArmorStand());
        }
        if (playerObject.getGadgetManager().hasState(GadgetStates.BALLOONS)) {
            BalloonGadget balloonGadget = (BalloonGadget) playerObject.getGadgetManager().getState(GadgetStates.BALLOONS);
            if (this.paperCore.getEntityHider().canSee(this.player, balloonGadget.getSheep()))
                this.paperCore.getEntityHider().hideEntity(this.player, balloonGadget.getSheep());
            if (this.paperCore.getEntityHider().canSee(this.player, balloonGadget.getArmorStand()))
                this.paperCore.getEntityHider().hideEntity(this.player, balloonGadget.getArmorStand());
        }
    }

    /**
     * @param key String of the property
     * @return returns null or a String
     */
    public String getProperty(String key) {
        if (this.iCloudPlayer.getProperties().contains(key))
            return this.iCloudPlayer.getProperties().getString(key);
        return null;
    }

    public ItemStack getPlayerHead() {
        ItemStack item = new ItemStack(Material.PLAYER_HEAD);
        SkullMeta itemMeta = (SkullMeta) item.getItemMeta();

        PlayerProfile profile = this.player.getPlayerProfile();
        itemMeta.setPlayerProfile(profile);
        item.setItemMeta(itemMeta);
        return item;
    }

    /**
     * @param key   String key of the prop
     * @param value String value of the prop
     */
    public void addProperty(String key, String value) {
        addProperty(this.getPlayer().getUniqueId(), key, value);
    }

    /**
     * @param key   String key of the prop
     * @param value String value of the prop
     */
    public void addProperty(UUID uuid, String key, String value) {
        ICloudPlayer d = BridgePlayerManager.getInstance().getOnlinePlayer(uuid);
        if(d != null) {
            d.getProperties().append(key, value);
            BridgePlayerManager.getInstance().updateOnlinePlayer(d);
        } else {
            ICloudOfflinePlayer x = BridgePlayerManager.getInstance().getOfflinePlayer(uuid);
            x.getProperties().append(key, value);
            BridgePlayerManager.getInstance().updateOfflinePlayer(x);
        }
    }

    /**
     * @param playerObject playerObject from paperCore
     * @return returns a boolean if the passed playerObject can see the current playerObject
     */
    public boolean canSee(PlayerObject playerObject) {
        if (playerObject == this)
            return true;
        return (this.visibility.equals(VISIBILITY.ALL) || (this.visibility.equals(VISIBILITY.VIP) && playerObject.player.hasPermission(this.paperCore.getConfigHandler().get("CORE", "permsVIP", "network.status.VIP")))) && (playerObject.visibility.equals(VISIBILITY.ALL) || (playerObject.visibility.equals(VISIBILITY.VIP) && this.player.hasPermission(this.paperCore.getConfigHandler().get("CORE", "permsVIP", "network.status.VIP"))));
    }


    /**
     * @param languageHandler languageHandler - normally initialised after config request
     * @param value           value => key from language
     * @return returns language object depending on the players language
     */
    public String getDefaultLang(LanguageHandler languageHandler, String value, String defaultValue) {
        return languageHandler.get(getDefaultLang(), value, defaultValue);
    }

    /**
     * @param inventoryName String - internal inventoryName
     * @return SmartInventory - inventory of the requested lang
     */
    public SmartInventory getDefaultInventory(String inventoryName) {
        return this.paperCore.getInventoryHandler().getInventory(getDefaultLang(), inventoryName);
    }

    /**
     * opens the requested Inventory
     *
     * @param inventoryName String . inventory name
     */
    public void openDefaultInventory(String inventoryName) {
        this.getDefaultInventory(inventoryName).open(this.player);
    }

    /**
     * @param itemName String - internal itemName
     * @return returns the proper item for the language
     */
    public ItemObject getDefaultItemObject(String itemName) {
        return this.paperCore.getItemHandler().getItem(getDefaultLang(), itemName);
    }

    /**
     * @return String - default language of player
     */
    public String getDefaultLang() {
        ICloudPlayer iCloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(this.getPlayer().getUniqueId());
        return iCloudPlayer.getProperties().getString("defaultLanguage");
    }

    /**
     * checks if default Properties are set, if not set them
     */
    private void checkDefaultProperties() {
        ICloudPlayer d = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());

        for (Map.Entry<String, String> entry : defaults.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            if (this.iCloudPlayer != null)
                if (!this.iCloudPlayer.getProperties().contains(key))
                    CloudUtils.setProperty(d, key, value);
        }
    }

    /**
     * adds default values to default HashMap, needet for PropertieDefaultCheck
     *
     * @return returns the defaultHashMap
     */
    private HashMap<String, String> addDefaults() {
        addDefaultToHashMap("defaultLanguage", "english");
        addDefaultToHashMap("rememberLobbyLocation", "false");
        addDefaultToHashMap("lastLobbyLocation", "none");
        addDefaultToHashMap("currentVisibility", "ALL");
        addDefaultToHashMap("currentBalloon", "none");
        addDefaultToHashMap("currentShoes", "none");
        addDefaultToHashMap("currentParticlesItem", "none");
        addDefaultToHashMap("currentParticlesParticles", "none");
        addDefaultToHashMap("currentCoins", this.startMoney);
        return this.defaults;
    }

    /**
     * checks and adds a single value to the DefaultHashMap
     *
     * @param defaultKey   String - Key name
     * @param defaultValue String - value Name
     * @return returns the defaultHashMap
     */
    public HashMap<String, String> addDefaultToHashMap(String defaultKey, String defaultValue) {
        if (!this.defaults.containsKey(defaultKey))
            this.defaults.put(defaultKey, defaultValue);
        return this.defaults;
    }

    /**
     * @return returns players texture as string
     */
    public String getTexture() {
        return texture;
    }

    /**
     * @return returns players signature as string
     */
    public String getSignature() {
        return signature;
    }

    /**
     * @param message String - Message to the player
     */
    @Override
    public void sendMessageToPlayer(String message) {
        ICloudPlayer cloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(this.getPlayer().getUniqueId()); //Returns an online cloudPlayers
        BridgePlayerManager.getInstance().proxySendPlayerMessage(cloudPlayer, message);
    }

    /**
     * @return returns bukkit player
     */
    @Override
    public Player getPlayer() {
        return this.player;
    }
}
