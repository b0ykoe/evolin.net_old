package net.evolin.papercore.objects;

import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import net.minecraft.server.v1_15_R1.NBTTagCompound;
import net.minecraft.server.v1_15_R1.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_15_R1.inventory.CraftItemStack;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

public class ItemObject implements Cloneable {
    private ItemStack itemStack;
    private ItemMeta itemMeta;
    private SkullMeta skullMeta;

    public Object clone() throws CloneNotSupportedException
    {
        return super.clone();
    }

    public ItemObject(ItemStack itemStack) {
        this.itemStack = itemStack.clone();
        this.itemMeta = itemStack.getItemMeta();
    }

    public ItemObject(Material material) {
        this.itemStack = new ItemStack(material);
        this.itemMeta = itemStack.getItemMeta();
        if(material.equals(Material.PLAYER_HEAD))
            this.skullMeta = (SkullMeta) this.itemMeta;
    }

    public ItemObject(Material material, int id) {
        this.itemStack = new ItemStack(material, 1, (short) id);
        this.itemMeta = itemStack.getItemMeta();
    }

    /**
     * https://minecraft-heads.com/custom-heads/search?searchword=Flag
     * @param textures
     * @return
     */
    public ItemObject setHeadTexture(String textures) {
        PlayerProfile profile = Bukkit.createProfile(UUID.randomUUID(), String.valueOf(System.currentTimeMillis()));;
        profile.setProperty(new ProfileProperty("textures", textures));
        this.skullMeta.setPlayerProfile(profile);
        return this;
    }

    public ItemObject setDisplayName(String name) {
        itemMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        if(this.skullMeta != null)
            skullMeta.setDisplayName(ChatColor.translateAlternateColorCodes('&', name));
        return this;
    }

    public ItemObject setArmorColor(Color color){
        if(itemMeta instanceof LeatherArmorMeta)
            ((LeatherArmorMeta) itemMeta).setColor(color);
        return this;
    }

    public Color getArmorColor(){
        if(itemMeta instanceof LeatherArmorMeta)
            ((LeatherArmorMeta) itemMeta).getColor();
        return Color.WHITE;
    }

    @SuppressWarnings("deprecation")
    public ItemObject setMetaID(byte metaID) {
        itemStack.getData().setData(metaID);
        return this;
    }

    public ItemObject setAmount(int amount) {
        this.itemStack.setAmount(amount);
        return this;
    }

    public ItemObject setDurability(short durability) {
        this.itemStack.setDurability(durability);
        return this;
    }

    public ItemObject addEnchantment(Enchantment enchantment, int lvl) {
        this.itemMeta.addEnchant(enchantment, lvl, false);
        if(this.skullMeta != null)
            this.itemMeta.addEnchant(enchantment, lvl, false);

        return this;
    }

    public ItemObject clearEnchantments() {
        this.itemMeta.getEnchants().forEach((enchantment, integer) -> this.itemMeta.removeEnchant(enchantment));
        if(this.skullMeta != null)
            this.skullMeta.getEnchants().forEach((enchantment, integer) -> this.skullMeta.removeEnchant(enchantment));
        return this;
    }

    public ItemObject removeEnchantment(Enchantment enchantment) {
        if (this.itemMeta.getEnchants().containsKey(enchantment))
            this.itemMeta.removeEnchant(enchantment);
        if(this.skullMeta != null && this.skullMeta.getEnchants().containsKey(enchantment))
            this.skullMeta.removeEnchant(enchantment);

        return this;
    }

    public ItemObject setLore(List<String> lines) {
        this.itemMeta.setLore(lines);
        if(this.skullMeta != null)
            this.skullMeta.setLore(lines);
        return this;
    }

    public ItemObject setLore(String lines) {
        ArrayList<String> lorelist = new ArrayList<String>();
        String[] lores = lines.split("\n");
        for (String addlore : lores) {
            lorelist.add((addlore.replaceAll("&([0-9a-f])", "\u00A7$1")).replaceAll("_", " "));
        }

        return this.setLore(lorelist);
    }

    public ItemObject setLore(String... lines) {
        this.itemMeta.setLore(Arrays.asList(lines));
        if(this.skullMeta != null)
            this.skullMeta.setLore(Arrays.asList(lines));
        return this;
    }


    public ItemObject resetLore() {
        this.itemMeta.getLore().clear();
        if(this.skullMeta != null)
            this.skullMeta.getLore().clear();
        return this;
    }

    public ItemStack hideEnchantments() {
        if(this.skullMeta != null)
            this.itemStack.setItemMeta(this.skullMeta);
        else
            this.itemStack.setItemMeta(this.itemMeta);
        net.minecraft.server.v1_15_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(this.itemStack);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        NBTTagList ench = new NBTTagList();
        tag.set("ench", ench);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }

    public ItemStack build() {
        if(this.skullMeta != null)
            this.itemStack.setItemMeta(this.skullMeta);
        else
            this.itemStack.setItemMeta(this.itemMeta);
        return itemStack;
    }
}
