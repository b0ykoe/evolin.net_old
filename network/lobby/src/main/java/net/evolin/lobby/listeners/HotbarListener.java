package net.evolin.lobby.listeners;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.lobby.Lobby;
import net.evolin.lobby.inventories.LobbyInventory;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;

public class HotbarListener implements Listener {

    private Lobby lobby;

    public HotbarListener(Lobby lobby) {
        this.lobby = lobby;
    }

    @EventHandler
    public void onPlayerJoin (PlayerJoinEvent event){
        Player player = event.getPlayer();
        ICloudPlayer iCloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());

        LobbyInventory.setDefaultHotbar(player, playerObject, this.lobby.paperCore.getLanguageHandler());
        Bukkit.getScheduler().runTaskLaterAsynchronously(this.lobby, () -> Bukkit.getScheduler().runTaskLater(lobby, () -> LobbyInventory.loadGadgetProperties(player, playerObject), 4), 4);
        event.setJoinMessage(null);
    }

    @EventHandler
    public void onPlayerQuit(PlayerQuitEvent playerQuitEvent) {
        playerQuitEvent.setQuitMessage(null);
    }


    @EventHandler
    public void onInteract (PlayerInteractEvent event)
    {
        Player player = event.getPlayer();
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());
        ItemStack currentItem = event.getItem();

        if(currentItem == null || currentItem.isSimilar(new ItemStack(Material.AIR))) {
            return;
        }
        if(!(event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)))
            return;

        final String currentName = currentItem.getItemMeta().getDisplayName();
        final String teleporter = playerObject.getDefaultItemObject("itemTeleporter").build().getItemMeta().getDisplayName();
        final String hider = playerObject.getDefaultItemObject("itemHide").build().getItemMeta().getDisplayName();
        final String profile = playerObject.getDefaultLang(this.lobby.paperCore.getLanguageHandler(), "itemProfileTitle", "§cProfile");
        final String gadgets = playerObject.getDefaultItemObject("itemGadgets").build().getItemMeta().getDisplayName();

        if(currentName.equalsIgnoreCase(teleporter))
            playerObject.getDefaultInventory("inventoryTeleporter").open(player);
        else if (currentName.equalsIgnoreCase(hider))
            playerObject.getDefaultInventory("inventoryHide").open(player);
        else if (currentName.equalsIgnoreCase(profile))
            playerObject.getDefaultInventory("inventoryProfile").open(player);
        else if (currentName.equalsIgnoreCase(gadgets))
            playerObject.getDefaultInventory("inventoryGadgets").open(player);
    }
}
