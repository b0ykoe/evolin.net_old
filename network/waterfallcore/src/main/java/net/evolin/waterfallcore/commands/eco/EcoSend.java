package net.evolin.waterfallcore.commands.eco;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.common.utils.SystemUtils;
import net.evolin.waterfallcore.WaterfallCore;
import net.evolin.waterfallcore.commands.SubCommand;
import net.evolin.common.objects.EcoObject;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class EcoSend extends SubCommand {
    public EcoSend(WaterfallCore waterfallCore) {
        super(waterfallCore, "send", "system.eco.send","send <name> <amount>", "Sendet eine Menge an Money");
    }

    @Override
    public void execute(ProxiedPlayer player, String[] args) {
        ProxiedPlayer sender = player;
        ProxiedPlayer receiver = null;
        ICloudPlayer iCloudPlayerSender = BridgePlayerManager.getInstance().getOnlinePlayer(sender.getUniqueId());
        ICloudPlayer iCloudPlayerReceiver = null;

        if(!this.hasPermission(player))
            return;

        if(!(EcoObject.getCoins(iCloudPlayerSender) >= SystemUtils.parseWithDefault(args[0], 0))) {
            player.sendMessage("Du hast nicht genügend Coins");
            return;
        }


        if(args.length == 0)
        return;

        if(args.length == 1) {
            player.sendMessage("Du hast keinen Spieler angegeben");
            return;
        }

        if(args.length == 2)
        receiver = ProxyServer.getInstance().getPlayer(args[0]);

        if(receiver == null) {
            player.sendMessage("Der Spieler %s ist nicht online".replace("%s", args[0]));
            return;
        }

        iCloudPlayerReceiver = BridgePlayerManager.getInstance().getOnlinePlayer(receiver.getUniqueId());
        if(iCloudPlayerReceiver.getName().equals(iCloudPlayerSender.getName())) {
            sender.sendMessage("Du kannst dir keine Coins schicken.");
            return;
        }

        EcoObject.addCoins(iCloudPlayerReceiver, SystemUtils.parseWithDefault(args[1], 0));
        EcoObject.removeCoins(iCloudPlayerSender, SystemUtils.parseWithDefault(args[1], 0));

        BridgePlayerManager.getInstance().proxySendPlayerMessage(iCloudPlayerSender, "Du hast %s %i Coins gesendet".replace("%i", args[1]).replace("%s", receiver.getName()));
        BridgePlayerManager.getInstance().proxySendPlayerMessage(iCloudPlayerReceiver, "Du hast %i Coins von %s bekommen".replace("%i", args[1]).replace("%s", sender.getName()));
    }
}
