package net.evolin.waterfallcore.commands;

import net.evolin.waterfallcore.WaterfallCore;
import net.md_5.bungee.api.connection.ProxiedPlayer;

import java.util.ArrayList;
import java.util.List;

public abstract class SubCommand {

    protected WaterfallCore waterfallCore;
    private String name, syntax, description, permission;
    private List<String> aliases;

    public SubCommand(WaterfallCore waterfallCore, String name, String syntax, String description) {
        this(waterfallCore, name, null, syntax, description);
    }

    public SubCommand(WaterfallCore waterfallCore, String name, String permission, String syntax, String description) {
        this.waterfallCore = waterfallCore;
        this.name = name;
        this.permission = permission;
        this.syntax = syntax;
        this.description = description;
        this.aliases = new ArrayList<>();
    }

    public boolean hasPermission(ProxiedPlayer player){
        if(this.permission == "" || this.permission == null)
            return true;

        return player.hasPermission(permission);
    }

    public abstract void execute(ProxiedPlayer player, String[] args);

    public String getName() {
        return this.name;
    }

    public String getSyntax() {
        return this.syntax;
    }

    public List<String> getAliases() {
        return this.aliases;
    }

    public String getDescription() {
        return this.description;
    }

    public String getPermission() {
        return this.permission;
    }

}
