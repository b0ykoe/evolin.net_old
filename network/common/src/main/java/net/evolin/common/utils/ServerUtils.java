package net.evolin.common.utils;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Collection;

public class ServerUtils {
    /**
     * shuts the server down if there are no more players on the server
     *
     * @param javaPlugin JavaPlugin - instance of the plugin
     */
    public static void shutdownServerIfNull(JavaPlugin javaPlugin) {
        if (javaPlugin.getServer().getOnlinePlayers().size() == 0)
            shutdownServer(javaPlugin);
    }

    /**
     * shuts the server down regardlessly of whatever
     *
     * @param javaPlugin JavaPlugin - instance of the plugin
     */
    private static void shutdownServer(JavaPlugin javaPlugin) {
        javaPlugin.getServer().shutdown();
    }

    /**
     * sends all players in a collection to a service
     *
     * @param players     Collection<? extends Player> - collection of players
     * @param serviceName string - name of the service (ex Lobby-1)
     */
    public static void sendAllPlayersTo(Collection<? extends Player> players, String serviceName) {
        for (Player p : players) {
            BridgePlayerManager.getInstance().proxySendPlayer(p.getUniqueId(), serviceName);
        }
    }

    /**
     * sends all players in a collection a message
     *
     * @param players Collection<? extends Player> - collection of players
     * @param message string - message to be send
     */
    public static void sendAllMessage(Collection<? extends Player> players, String message) {
        for (Player p : players) {
            p.sendMessage(new TextComponent(message));
        }
    }

    /**
     * safes properties to a collected playerlist from the current server to prepare a forced swap
     *
     * @param players     Collection<? extends Player> - collection of players
     * @param serviceName string - target servicename (ex. Lobby-2)
     * @param swapProp    string - true if the player should be forced to load those properties
     */
    public static void safeSwapProperties(Collection<? extends Player> players, String serviceName, String swapProp) {
        for (Player p : players) {
            ICloudPlayer cloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(p.getUniqueId());

            if (cloudPlayer != null) {
                cloudPlayer.getProperties()
                        .append(swapProp, true)
                        .append("swapTo", serviceName)
                        .append("playerGameMode", p.getGameMode())
                        .append("playerAllowFlight", p.getAllowFlight())
                        .append("playerIsFlying", p.isFlying())
                        .append("playerExperience", p.getExp())
                        .append("playerLocation", ConfigUtils.locationToString(p.getLocation()));
                BridgePlayerManager.getInstance().updateOnlinePlayer(cloudPlayer);
            }
        }
    }

    /**
     * safes properties to a single player from the current server to prepare a forced swap
     *
     * @param player      player - playerinstance
     * @param serviceName string - target servicename (ex. Lobby-2)
     * @param swapProp    string - true if the player should be forced to load those properties
     */
    public static void safeSwapPropertie(Player player, String serviceName, String swapProp) {
        ICloudPlayer cloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());

        if (cloudPlayer != null) {
            cloudPlayer.getProperties()
                    .append(swapProp, true)
                    .append("swapTo", serviceName)
                    .append("playerGameMode", player.getGameMode())
                    .append("playerAllowFlight", player.getAllowFlight())
                    .append("playerIsFlying", player.isFlying())
                    .append("playerExperience", player.getExp())
                    .append("playerLocation", ConfigUtils.locationToString(player.getLocation()));
            BridgePlayerManager.getInstance().updateOnlinePlayer(cloudPlayer);
        }
    }

    /**
     * loads the properties from a player to load after the forced serverswap
     *
     * @param player player - playerinstance
     */
    public static void loadSwapProperties(Player player) {
        ICloudPlayer cloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());
        if (cloudPlayer != null) {
            player.setGameMode(cloudPlayer.getProperties().get("playerGameMode", GameMode.class));
            player.setAllowFlight(cloudPlayer.getProperties().getBoolean("playerAllowFlight"));
            player.setFlying(cloudPlayer.getProperties().getBoolean("playerIsFlying"));
            player.setExp(cloudPlayer.getProperties().getInt("playerExperience"));
            player.teleport(ConfigUtils.locationFromString(cloudPlayer.getProperties().getString("playerLocation")));

            cloudPlayer.getProperties().remove("forceSwap");
            cloudPlayer.getProperties().remove("lobbySwap");
            cloudPlayer.getProperties().remove("swapTo");
            cloudPlayer.getProperties().remove("playerGameMode");
            cloudPlayer.getProperties().remove("playerAllowFlight");
            cloudPlayer.getProperties().remove("playerIsFlying");
            cloudPlayer.getProperties().remove("playerExperience");
            cloudPlayer.getProperties().remove("playerLocation");
            BridgePlayerManager.getInstance().updateOnlinePlayer(cloudPlayer);
        }
    }

    /**
     * forces a single player to connect to a specific service
     *
     * @param player      player - playerinstance
     * @param serviceName string - name of the service (ex. Lobby-1)
     */
    public static void sendPlayerTo(Player player, String serviceName) {
        BridgePlayerManager.getInstance().proxySendPlayer(player.getUniqueId(), serviceName);
    }
}
