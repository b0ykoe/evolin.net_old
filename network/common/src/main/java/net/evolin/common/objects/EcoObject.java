package net.evolin.common.objects;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.common.utils.CloudUtils;
import net.evolin.common.utils.SystemUtils;

public class EcoObject {

    public static int addCoins(ICloudPlayer iCloudPlayer, int amount) {
        int coins = SystemUtils.parseWithDefault(iCloudPlayer.getProperties().getString("currentCoins"), 0);
        coins += amount;
        CloudUtils.setProperty(iCloudPlayer, "currentCoins", coins);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateScoreboard", "null", new JsonDocument().append("uuid", iCloudPlayer.getUniqueId()));
        return coins;
    }

    public static int removeCoins(ICloudPlayer iCloudPlayer, int amount) {
        int coins = SystemUtils.parseWithDefault(iCloudPlayer.getProperties().getString("currentCoins"), 0);
        coins -= amount;
        CloudUtils.setProperty(iCloudPlayer, "currentCoins", coins);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateScoreboard", "null", new JsonDocument().append("uuid", iCloudPlayer.getUniqueId()));
        return coins;
    }

    public static int setCoins(ICloudPlayer iCloudPlayer, int amount) {
        CloudUtils.setProperty(iCloudPlayer, "currentCoins", amount);
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("updateScoreboard", "null", new JsonDocument().append("uuid", iCloudPlayer.getUniqueId()));
        return amount;
    }

    public static int getCoins(ICloudPlayer iCloudPlayer) {
        return SystemUtils.parseWithDefault(iCloudPlayer.getProperties().getString("currentCoins"), 0);
    }
}

