package net.evolin.papercore.listener;

import com.google.gson.Gson;
import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.handler.InventoryHandler;
import net.evolin.papercore.handler.ItemHandler;

public class ConfigListener {

    private final PaperCore paperCore;

    /**
     * basic Listener constructor
     * @param paperCore passes trough the PaperCore Plugin to access objects
     */
    public ConfigListener(PaperCore paperCore) {
        this.paperCore = paperCore;
    }

    /**
     * waits for the module to send the current configurations
     * @param event ChannelMessageReceiveEvent from CloudNet
     */
    @EventListener
    public void handleChannelMessage(ChannelMessageReceiveEvent event) {
        if (event.getChannel().equalsIgnoreCase("receiveConfig")) {
            if ("config".equalsIgnoreCase(event.getMessage()))
                this.paperCore.setConfigHandler(new Gson().fromJson(event.getData().toJson(), ConfigHandler.class));
            if ("messages".equalsIgnoreCase(event.getMessage()))
                this.paperCore.setLanguageHandler(new Gson().fromJson(event.getData().toJson(), net.evolin.common.handler.LanguageHandler.class));

            if(this.paperCore.getLanguageHandler() != null && this.paperCore.getConfigHandler() != null) {
                this.paperCore.setItemHandler(new ItemHandler(this.paperCore));
                this.paperCore.setInventoryHandler(new InventoryHandler(this.paperCore));
            }

            CloudNetDriver.getInstance().getMessenger().sendChannelMessage("reloadInit", this.paperCore.getServiceId().getName(), new JsonDocument());
        }
    }
}
