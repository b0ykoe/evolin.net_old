package net.evolin.lobby.listeners;

import net.evolin.lobby.Lobby;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.gadgets.gadgets.BalloonGadget;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.entity.Sheep;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.entity.*;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.weather.WeatherChangeEvent;

public class CancelListener implements Listener {

    private Lobby lobby;

    public CancelListener(Lobby lobby) {
        this.lobby = lobby;
    }

    /**
     * disables block breaking
     * @param event BlockBreakEvent
     */
    @EventHandler
    public void handleBreak(BlockBreakEvent event) {
        Player player = event.getPlayer();
        if(!player.getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * disables block placing
     * @param event BlockPlaceEvent
     */
    @EventHandler
    public void handlePlace(BlockPlaceEvent event) {
        Player player = event.getPlayer();
        if(!player.getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * disables item dropping
     * @param event PlayerDropItemEvent
     */
    @EventHandler
    public void handleItemDrop(PlayerDropItemEvent event) {
        Player player = event.getPlayer();
        if(!player.getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * disables item pickup
     * @param event EntityPickupItemEvent
     */
    @EventHandler
    public void handleItemPickup(EntityPickupItemEvent event) {
        if(!(event.getEntity() instanceof Player))
            return;

        Player player = ((Player) event.getEntity());
        if (!player.getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * disables damage
     * @param event EntityDamageEvent
     */
    @EventHandler
    public void  handleDamage(EntityDamageEvent event) {
        if(!event.getCause().equals(EntityDamageEvent.DamageCause.CUSTOM))
            event.setCancelled(true);
    }

    /**
     * disables damage from entities
     * @param event EntityDamageByEntityEvent
     */
    @EventHandler
    public void handleDamageByEntity(EntityDamageByEntityEvent event) {
        if(!(event.getDamager() instanceof Player)) {
            event.setCancelled(true);
            return;
        }

        if(!(event.getEntity() instanceof Player))
            return;

        Player player = ((Player) event.getEntity());
        if (!player.getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * disables damage from blocks
     * @param event EntityDamageByBlockEvent
     */
    @EventHandler
    public void handleDamageByBlocks(EntityDamageByBlockEvent event) {
        if(!(event.getEntity() instanceof Player))
            return;

        Player player = ((Player) event.getEntity());
        if (!player.getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * disables mob/animal spawning
     * @param event CreateSpawnEvent
     */
    @EventHandler
    public void handleSpawn (CreatureSpawnEvent event) {
        if(!(event.getSpawnReason().equals(CreatureSpawnEvent.SpawnReason.CUSTOM)))
            event.setCancelled(true);
    }

    /**
     * cancels the weather change
     * @param event WeatherChangeEvent
     */
    @EventHandler
    public void handleWeather (WeatherChangeEvent event) {
        event.setCancelled(true);
    }

    /**
     * cancel interacts
     * @param event PlayerInteractEvent
     */
    @EventHandler
    public void handleInteract (PlayerInteractEvent event)
    {
        if(!event.getPlayer().getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * updates the inventory after closing
     * @param event InventoryCloseEvent
     */
    @EventHandler
    public void onInventoryClose (InventoryCloseEvent event){
        if(event.getPlayer() instanceof Player)
            Bukkit.getScheduler().runTaskLaterAsynchronously(lobby, () -> ((Player) event.getPlayer()).updateInventory(), 1L);
    }

    /**
     * cancels the leashing of entitys
     * @param event PlayerLeashEntityEvent
     */
    @EventHandler
    public void handleLeash (PlayerLeashEntityEvent event)
    {
        if(!event.getPlayer().getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * cancels inventory clicking
     * @param event InventoryClickEvent
     */
    @EventHandler(priority = EventPriority.LOWEST)
    public void handleInventoryClick (InventoryClickEvent event)
    {
        if(!event.getWhoClicked().getGameMode().equals(GameMode.CREATIVE))
            event.setCancelled(true);
    }

    /**
     * cancels leave decaying
     * @param event LeavesDecayEvent
     */
    @EventHandler
    public void handleLeaveDecay(LeavesDecayEvent event) {
        event.setCancelled(true);
    }

    /**
     * cancel unleashing
     * @param event PlayerInteractEntityEvent
     */
    @EventHandler
    public void handleUnleash (PlayerInteractEntityEvent event) {
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(event.getPlayer().getUniqueId());
        event.setCancelled(true);

        if (event.getRightClicked() instanceof Sheep)
            if (playerObject.getGadgetManager().hasState(GadgetStates.BALLOONS)) {
                if (playerObject.getGadgetManager().getState(GadgetStates.BALLOONS) instanceof BalloonGadget) {
                    BalloonGadget balloonGadget = (BalloonGadget) playerObject.getGadgetManager().getState(GadgetStates.BALLOONS);
                    if (event.getRightClicked().getUniqueId().equals(balloonGadget.getSheep().getUniqueId()))
                        ((Sheep) event.getRightClicked()).setLeashHolder(event.getPlayer());
                }
            }
    }
}
