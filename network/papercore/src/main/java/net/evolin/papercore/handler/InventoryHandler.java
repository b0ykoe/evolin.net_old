package net.evolin.papercore.handler;

import fr.minuskube.inv.SmartInventory;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.inventories.LanguageInventory;
import net.evolin.papercore.inventories.LobbySettingsInventory;
import net.evolin.papercore.inventories.ProfileInventory;
import net.evolin.papercore.inventories.SettingsInventory;
import org.bukkit.event.inventory.InventoryType;

import java.util.HashMap;
import java.util.Map;

public class InventoryHandler {

    // 1. HashMap <String, HashMap> - LanguageName <HashMap> - ex. English - <HashMap>
    // 2. HashMap <String, SmartInventory> - InventoryName Inventory - ex. ConfigInventory SmartInventory
    private HashMap<String, HashMap<String, SmartInventory>> inventoryMap = new HashMap<>();
    private LanguageHandler languageHandler;
    private String defaultLanguage;
    private PaperCore paperCore;

    /**
     *
     * @param paperCore
     */
    public InventoryHandler(PaperCore paperCore) {
        this.paperCore = paperCore;
        this.languageHandler = this.paperCore.getLanguageHandler();
        this.defaultLanguage = this.languageHandler.defaultLanguage;

        // inits all default inventories in their default languages
        for(Map.Entry<String, Map<String, Map<String, String>>> entry : this.languageHandler.languages.entrySet()) {
            String key = entry.getKey();
            init(key);
        }
    }

    /**
     * inits inventories for that specific language
     * @param language String language for the inventories
     */
    public void init(String language) {
        this.addInventory(language, "inventorySettings", createInventory("inventorySettings", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventory-title-settings", "§cSettings"), true, new SettingsInventory(this.paperCore)));
        this.addInventory(language, "inventoryLanguage", createInventory("inventoryLanguage", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventory-title-language", "§cLanguage Settings"), true, new LanguageInventory(this.paperCore)));
        this.addInventory(language, "inventoryLobbySettings", createInventory("inventoryLobbySettings", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventory-title-lobby", "§cLobby Settings"), true, new LobbySettingsInventory(this.paperCore)));
        this.addInventory(language, "inventoryProfile", createInventory("inventoryProfile", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventory-title-profile", "§cProfile"), true, new ProfileInventory(this.paperCore)));
    }

    /**
     *
     * @param id
     * @param rows
     * @param columns
     * @param type
     * @param title
     * @param closeable
     * @param provider
     * @return
     */
    public SmartInventory createInventory(String id, Integer rows, Integer columns, InventoryType type, String title, boolean closeable, InventoryProvider provider) {
        return SmartInventory.builder()
                .id(id)
                .provider(provider)
                .size(rows, columns)
                .type(type)
                .title(title)
                .closeable(closeable)
                .build();
    }

    /**
     * adds a inventory to the HashMap
     * @param language String - language of the inventory
     * @param inventoryName String - name of the inventory
     * @param inventory SmartInventory - inventory
     */
    public void addInventory(String language, String inventoryName, SmartInventory inventory) {
        // if HashMap does not contain language (usually happens on the first inventories)
        if(!this.inventoryMap.containsKey(language))
            this.inventoryMap.put(language, new HashMap<>());
        this.inventoryMap.get(language).put(inventoryName, inventory);
    }

    /**
     * returns the inventory requested from lang
     * @param language language of the inventory
     * @param name internal name of the inventory
     * @return null or the inventory or the fallback inventory (if the language does not have the inventory)
     */
    public SmartInventory getInventory(String language, String name) {
        // if language exist in map
        if(this.inventoryMap.containsKey(language))
            // if language does contain the inventory
            if(this.inventoryMap.get(language).containsKey(name))
                return this.inventoryMap.get(language).get(name);
        // if default language does contain the inventory return
        if(this.inventoryMap.get(this.defaultLanguage).containsKey(name))
            return getInventory(this.defaultLanguage, name);
        // else return nothing
        return null;
    }
}
