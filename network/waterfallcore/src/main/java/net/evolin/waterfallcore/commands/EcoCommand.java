package net.evolin.waterfallcore.commands;

import net.evolin.waterfallcore.WaterfallCore;
import net.evolin.waterfallcore.commands.eco.*;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class EcoCommand extends Command {

    private WaterfallCore waterfallCore;

    private List<SubCommand> subCommands;
    private SubCommand helpCommand;

    public EcoCommand(WaterfallCore waterfallCore, String name, String permission, String... aliases) {
        super(name, permission, aliases);
        this.waterfallCore = waterfallCore;
        this.subCommands = new ArrayList<>();
        this.subCommands.add(new EcoSet(this.waterfallCore));
        this.subCommands.add(new EcoRemove(this.waterfallCore));
        this.subCommands.add(new EcoAdd(this.waterfallCore));
        this.subCommands.add(new EcoSend(this.waterfallCore));
        this.subCommands.add(new EcoInfo(this.waterfallCore));
        this.subCommands.add(this.helpCommand = new EcoHelp(this.waterfallCore, this.subCommands));
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {
        ProxiedPlayer sender = (ProxiedPlayer) commandSender;

        if(args.length == 0) {
            this.helpCommand.execute(sender, null);
            return;
        }

        // Find command with given name
        Optional<SubCommand> commandOptional = this.subCommands.stream().filter(command -> command.getName().equals(args[0]) || command.getAliases().contains(args[0].toLowerCase())).findFirst();
        // Remove first element of array and execute
        commandOptional.ifPresent(subCommand -> subCommand.execute(sender, Arrays.copyOfRange(args, 1, args.length)));
    }

}
