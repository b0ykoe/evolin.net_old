package net.evolin.citybuild;

import net.evolin.citybuild.listeners.PlayerListener;
import net.evolin.papercore.PaperCore;
import org.bukkit.plugin.java.JavaPlugin;

public final class Citybuild extends JavaPlugin {

    public PaperCore paperCore;

    @Override
    public void onEnable() {
        this.paperCore = PaperCore.getInstance();
        this.getServer().getPluginManager().registerEvents(new PlayerListener(this), this);

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
