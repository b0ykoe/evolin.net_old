package net.evolin.discord.sockets;

import net.evolin.common.simpleServerClient.Datapackage;
import net.evolin.discord.Discord;

import java.net.Socket;

public class Server extends net.evolin.common.simpleServerClient.Server {

    private Discord discord;

    /**
     * overrides the server, also sets the logger
     * @param port INT for the socket port
     */
    public Server(int port, Discord discord) {
        // int port, boolean autoRegisterEveryClient, boolean keepConnectionAlive, boolean useSSL, boolean muted
        super(port, true, true, false, true);
        // sets the logger for future usage, static contents are bad!
        setLogger(Discord.logger);
        getLogger().info("Socket server started..");
        // initializing discord
        this.discord = discord;
    }

    /**
     * Overrides the preStart method, registering "Channels" here
     */
    @Override
    public void preStart() {
        // register command channel
        registerMethod("command", (msg, socket) -> {
            getLogger().debug("Sending %socket% callback packet".replace("%socket%", msg.getSenderID()));
            sendReply(socket, "ok");
            getLogger().info("Socket {%socket%} executed the command {%command%}".replace("%socket%", msg.getSenderID()).replace("%command%",(String) msg.get(1)));
        });
        // have to use a static context because the preStart will be called before the Server since its a @Override
        Discord.logger.info("Trying to start the Socket server..");
    }

    /**
     *
     * @param msg The message the client registered with
     * @param socket The socket the client registered with. Be careful with this! You should not close
     *        this socket, because the server should have stored it normally to reach this client
     */
    @Override
    public void onClientRegistered(Datapackage msg, Socket socket) {
        super.onClientRegistered();
        getLogger().info(this.discord.languageHandler.get("client-registered-event", "Client {%client%} connected on our Socket.").replace("%client%", msg.getSenderID()));
    }

    /**
     *
     * @param remoteClient The client that was removed from the list of reachable clients
     */
    @Override
    public void onClientRemoved(RemoteClient remoteClient) {
        super.onClientRemoved(remoteClient);
        getLogger().info(this.discord.languageHandler.get("client-removed-event", "Client {%client%} disconnected from our Socket.").replace("%client%", remoteClient.getId()));
    }
}
