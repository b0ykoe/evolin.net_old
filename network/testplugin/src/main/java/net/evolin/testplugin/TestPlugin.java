package net.evolin.testplugin;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.*;
import com.destroystokyo.paper.profile.PlayerProfile;
import com.destroystokyo.paper.profile.ProfileProperty;
import com.google.common.collect.Lists;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.evolin.common.packets.WrapperPlayServerEntityDestroy;
import net.evolin.common.packets.WrapperPlayServerNamedEntitySpawn;
import net.evolin.common.packets.WrapperPlayServerPlayerInfo;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public final class TestPlugin extends JavaPlugin implements Listener {

    //PacketPlayOutEntityDestroy
    //PacketPlayOutPlayerInfo
    //PacketPlayOutNamedEntitySpawn

    ProtocolManager manager;

    @Override
    public void onEnable() {
        // Plugin startup logic
        getServer().getPluginManager().registerEvents(this, this);
        manager = ProtocolLibrary.getProtocolManager();

        manager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, WrapperPlayServerNamedEntitySpawn.TYPE) {
            @Override
            public void onPacketSending(PacketEvent event) {
                WrapperPlayServerNamedEntitySpawn wrapperPlayServerNamedEntitySpawn = new WrapperPlayServerNamedEntitySpawn(event.getPacket());
                PlayerObject playerObject = PaperCore.getInstance().getUuidPlayerObject(wrapperPlayServerNamedEntitySpawn.getPlayerUUID());
                if(playerObject != null && playerObject.isNicked()) {
                    wrapperPlayServerNamedEntitySpawn.setPlayerUUID(playerObject.fakeUUID);
                    event.getPacket().getUUIDs().write(0, wrapperPlayServerNamedEntitySpawn.getPlayerUUID());
                }
            }
        });

        manager.addPacketListener(new PacketAdapter(this, ListenerPriority.NORMAL, WrapperPlayServerPlayerInfo.TYPE) {
            @Override
            public void onPacketSending(PacketEvent event) {
                WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfo = new WrapperPlayServerPlayerInfo(event.getPacket());
                if(wrapperPlayServerPlayerInfo.getAction().equals(EnumWrappers.PlayerInfoAction.ADD_PLAYER)) {
                    List<PlayerInfoData> list2 = Lists.newArrayList();
                    boolean send = false;
                    for (PlayerInfoData playerInfoData : wrapperPlayServerPlayerInfo.getData()) {
                        PlayerObject playerObject = PaperCore.getInstance().getUuidPlayerObject(playerInfoData.getProfile().getUUID());
                        if (playerObject != null && playerObject.isNicked()) {
                            WrappedChatComponent displayName2 = WrappedChatComponent.fromText(ChatColor.translateAlternateColorCodes('&', playerObject.getDisplayDependsNick()) + playerObject.getCurrentProfile().getName());
                            WrappedGameProfile wrappedGameProfile2 = new WrappedGameProfile(playerObject.fakeUUID, playerObject.getCurrentProfile().getName());
                            String texture = "";
                            String signature = "";
                            if (playerObject.getCurrentProfile() != null) {
                                for (ProfileProperty profileProperty : playerObject.getCurrentProfile().getProperties()) {
                                    texture = profileProperty.getValue();
                                    signature = profileProperty.getSignature();
                                }
                            }
                            wrappedGameProfile2.getProperties().get("texture").clear();
                            wrappedGameProfile2.getProperties().get("texture").add(new WrappedSignedProperty("textures", texture, signature));
                            PlayerInfoData newInfoData2 = new PlayerInfoData(wrappedGameProfile2, playerObject.getPlayer().spigot().getPing(), EnumWrappers.NativeGameMode.valueOf(playerObject.getPlayer().getGameMode().toString()), displayName2);
                            list2.add(newInfoData2);
                            send = !event.getPlayer().getUniqueId().equals(playerObject.getPlayer().getUniqueId());
                        }
                    }
                    if (send)
                        event.getPacket().getPlayerInfoDataLists().write(0, list2);
                }
            }
        });
    }


    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    @EventHandler
    public void test(AsyncPlayerChatEvent event) {
        if(event.getMessage().length() >= 2)
            if (event.getMessage().startsWith("#test"))
                testnick(event.getPlayer(), event.getMessage().split(" ")[1]);
    }


    @EventHandler(priority = EventPriority.HIGHEST)
    public void onPlayerJoin(PlayerJoinEvent event) {
        event.setJoinMessage(null);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerQuit(PlayerQuitEvent event) {
        PlayerObject obj = PaperCore.getInstance().getUuidPlayerObject(event.getPlayer().getUniqueId());
        if(obj.isNicked()) {
            for (Player receivePlayer : this.getServer().getOnlinePlayers()) {
                List<PlayerInfoData> list = Lists.newArrayList();
                WrappedChatComponent displayName = WrappedChatComponent.fromText(ChatColor.translateAlternateColorCodes('&', obj.getDisplayDependsNick()) + "GERGuardian");
                WrappedGameProfile wrappedGameProfile = new WrappedGameProfile(obj.fakeUUID, event.getPlayer().getName());
                PlayerInfoData newInfoData = new PlayerInfoData(wrappedGameProfile, event.getPlayer().spigot().getPing(), EnumWrappers.NativeGameMode.valueOf(event.getPlayer().getGameMode().toString()), displayName);
                list.add(newInfoData);

                // send player info
                WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfoRemove = new WrapperPlayServerPlayerInfo();
                wrapperPlayServerPlayerInfoRemove.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
                wrapperPlayServerPlayerInfoRemove.setData(list);

                try {
                    manager.sendServerPacket(receivePlayer, wrapperPlayServerPlayerInfoRemove.getHandle());
                } catch (InvocationTargetException e) {
                    e.printStackTrace();
                }
            }
        }
        event.setQuitMessage(null);
    }

    public void testnick(Player player, String name) {
        //UUID fakeUUID = UUID.fromString("886a2c82-ddb6-42b0-98fa-071aca3f577f");
        PlayerObject obj = PaperCore.getInstance().getUuidPlayerObject(player.getUniqueId());
        obj.setNicked(true);

        this.getServer().getScheduler().runTaskLater(this, () -> {
            String texture = "eyJ0aW1lc3RhbXAiOjE1ODIyOTkyOTcyNDQsInByb2ZpbGVJZCI6ImZkNjBmMzZmNTg2MTRmMTJiM2NkNDdjMmQ4NTUyOTlhIiwicHJvZmlsZU5hbWUiOiJSZWFkIiwic2lnbmF0dXJlUmVxdWlyZWQiOnRydWUsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9jMGVjYjk3MmFhZDM3YTJlMzk2ODFjMmY4ZmI4MmViNWVjOWE0NjdjZGFkMTUzZTQwMTcxYmE1YjQ5ZDQzMzE0In19fQ==";
            String signature = "dFCYLMIU0VI4VxvS9SSeBAFrLVsyqewbCBWbYfCkUhqolxON/oKQCp84V0ZoGt3Xj2+mXU7EMyIIJrMeRXFmSf5/DhTrql+bv2vhifYD+savW/cPrXX1pSraYtuF7OVBjoW+i2qiAAaXdKBWGLi851e409Lta01pYGU1xYv97fHCeFDbNmjD53BvF92VqFZBzyqkzKvHZ7cnioU0zqCkOt7Noz95UE3IUCSl6aOAXcNSWalvsZl3N9t7mMgJko0qfXvMm0BudXljcUimnlTCqu7hVlcr+PZfY97CrzQJ4cJxaE26z8gHNgdaVynpKBEAb0web/1WLkUULhIm9XDM10K3/BkNRxvsQZ9KHCIXMuUYCZyjLhUcp6LNyinN2WDyQb3rEmdiNtpE6DBjjZIgOiE9Sf8fj2Xd/LybYkWgu2E1I6ije2d3okkiTb31QS8vflqpqtzC1Y1UKNqKxEJF+6FtRCP49EHn5xMBrNn4eDzfS7O3Vvvl7SowiwPgKh1aJdUIOdkPtyqVW0pAJ97rYwejsCDUFAOsvrNLarq9ZI9FK0h9YlFkmkVegdiJXgtv1TQl5dm+ZaVHxgFBDs9q865pCX+uno8ElfVMS335LoteKoDpkk1XtBSt6+GZCGxysB2ZxXCY5ePNoA6XqMoq/lOibAepXTvdz8822u/bZbs=";

            GameProfile profile = null;
            try {
                profile = GameProfileBuilder.fetch(UUIDFetcher.getUUID(name));
            } catch (IOException e) {
                e.printStackTrace();
            }


            if (profile != null) {
                for (Property prop : profile.getProperties().get("textures")) {
                    texture = prop.getValue();
                    signature = prop.getSignature();
                }
            }

            obj.fakeUUID = Objects.requireNonNull(profile).getId();


            PlayerProfile playerProfile = Bukkit.createProfile(player.getUniqueId());
            playerProfile.setProperty(new ProfileProperty("textures", texture, signature));
            playerProfile.setName(profile.getName());
            playerProfile.complete(false);
            player.setPlayerProfile(playerProfile);

            obj.setCurrentProfile(playerProfile);

            player.setCustomName(profile.getName());
            player.setDisplayName(profile.getName());
            player.setPlayerListName(profile.getName());

            player.spigot().respawn();

            // orig player (remove tablist)
            List<PlayerInfoData> list = Lists.newArrayList();
            UUID uuid = player.getUniqueId();
            WrappedChatComponent displayName = WrappedChatComponent.fromText(ChatColor.translateAlternateColorCodes('&', obj.getDisplayDependsNick()) + player.getName());
            WrappedGameProfile wrappedGameProfile = new WrappedGameProfile(uuid, player.getName());
            PlayerInfoData newInfoData = new PlayerInfoData(wrappedGameProfile, player.spigot().getPing() , EnumWrappers.NativeGameMode.valueOf(player.getGameMode().toString()), displayName);
            list.add(newInfoData);

            // fakeplayer (add tablist)
            List<PlayerInfoData> list2 = Lists.newArrayList();
            WrappedChatComponent displayName2 = WrappedChatComponent.fromText(ChatColor.translateAlternateColorCodes('&', obj.getDisplayDependsNick()) + profile.getName());
            WrappedGameProfile wrappedGameProfile2 = new WrappedGameProfile(profile.getId(), profile.getName());
            wrappedGameProfile2.getProperties().get("texture").clear();
            wrappedGameProfile2.getProperties().get("texture").add(new WrappedSignedProperty("textures", texture, signature));
            PlayerInfoData newInfoData2 = new PlayerInfoData(wrappedGameProfile2, player.spigot().getPing() , EnumWrappers.NativeGameMode.valueOf(player.getGameMode().toString()), displayName2);
            list2.add(newInfoData2);

            for(Player receivePlayer : this.getServer().getOnlinePlayers()) {
                if(player.getUniqueId() != receivePlayer.getUniqueId()) {
                    // despawn player
                    WrapperPlayServerEntityDestroy wrapperPlayServerEntityDestroy = new WrapperPlayServerEntityDestroy();
                    int[] entityIds = {player.getEntityId()};
                    wrapperPlayServerEntityDestroy.setEntityIds(entityIds);

                    // send player info
                    // remove from tablist
                    WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfoRemove = new WrapperPlayServerPlayerInfo();
                    wrapperPlayServerPlayerInfoRemove.setAction(EnumWrappers.PlayerInfoAction.REMOVE_PLAYER);
                    wrapperPlayServerPlayerInfoRemove.setData(list);

                    // add to tablist
                    WrapperPlayServerPlayerInfo wrapperPlayServerPlayerInfoAdd = new WrapperPlayServerPlayerInfo();
                    wrapperPlayServerPlayerInfoAdd.setAction(EnumWrappers.PlayerInfoAction.ADD_PLAYER);
                    wrapperPlayServerPlayerInfoAdd.setData(list2);

                    // spawn player
                    WrapperPlayServerNamedEntitySpawn wrapperPlayServerNamedEntitySpawn = new WrapperPlayServerNamedEntitySpawn();
                    wrapperPlayServerNamedEntitySpawn.setPlayerUUID(obj.fakeUUID = profile.getId());
                    wrapperPlayServerNamedEntitySpawn.setEntityID(player.getEntityId());
                    wrapperPlayServerNamedEntitySpawn.setPitch(player.getLocation().getPitch());
                    wrapperPlayServerNamedEntitySpawn.setPosition(player.getLocation().getDirection());
                    wrapperPlayServerNamedEntitySpawn.setX(player.getLocation().getX());
                    wrapperPlayServerNamedEntitySpawn.setY(player.getLocation().getY());
                    wrapperPlayServerNamedEntitySpawn.setZ(player.getLocation().getZ());
                    wrapperPlayServerNamedEntitySpawn.setYaw(player.getLocation().getYaw());

                    try {
                        manager.sendServerPacket(receivePlayer, wrapperPlayServerEntityDestroy.getHandle());
                        manager.sendServerPacket(receivePlayer, wrapperPlayServerPlayerInfoRemove.getHandle());
                        manager.sendServerPacket(receivePlayer, wrapperPlayServerPlayerInfoAdd.getHandle());
                        manager.sendServerPacket(receivePlayer, wrapperPlayServerNamedEntitySpawn.getHandle());
                    } catch (InvocationTargetException e) {
                        e.printStackTrace();
                    }

                }
            }
        }, 2);

        this.getServer().getScheduler().runTaskLater(this, obj::updateNameTags, 4);

        //PacketPlayOutEntityDestroy
        //PacketPlayOutPlayerInfo
        //PacketPlayOutNamedEntitySpawn


    }
}
