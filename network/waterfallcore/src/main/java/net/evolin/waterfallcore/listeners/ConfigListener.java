package net.evolin.waterfallcore.listeners;

import com.google.gson.Gson;
import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.waterfallcore.WaterfallCore;

public class ConfigListener {

    private final WaterfallCore waterfallCore;

    /**
     * basic Listener constructor
     * @param waterfallCore passes trough the PaperCore Plugin to access objects
     */
    public ConfigListener(WaterfallCore waterfallCore) {
        this.waterfallCore = waterfallCore;
    }

    /**
     * waits for the module to send the current configurations
     * @param event ChannelMessageReceiveEvent from CloudNet
     */
    @EventListener
    public void handleChannelMessage(ChannelMessageReceiveEvent event) {
        if (event.getChannel().equalsIgnoreCase("receiveConfig")) {
            if ("config".equalsIgnoreCase(event.getMessage()))
                this.waterfallCore.configHandler = new Gson().fromJson(event.getData().toJson(), ConfigHandler.class);
            if ("messages".equalsIgnoreCase(event.getMessage()))
                this.waterfallCore.languageHandler = new Gson().fromJson(event.getData().toJson(), LanguageHandler.class);
        }
    }
}
