package net.evolin.waterfallcore.commands.eco;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

public class PayCommand extends Command {
    public PayCommand(String name) {
        super(name);
    }

    @Override
    public void execute(CommandSender sender, String[] args) {
        if(args.length == 0 || args.length == 1)
            return;

        if(args.length == 2)
            ProxyServer.getInstance().getPluginManager().dispatchCommand(sender, "eco send " + args[0] + " " + args[1]);
    }
}
