package net.evolin.lobby.inventories.gadgets;

import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.lobby.Lobby;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.inventories.InventoryUtils;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.entity.Player;

public class GadgetInventory implements InventoryProvider {

    private Lobby lobby;
    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;
    private ItemHandler itemHandler;

    public GadgetInventory(Lobby lobby) {
        this.lobby = lobby;
        this.configHandler = this.lobby.paperCore.getConfigHandler();
        this.languageHandler = this.lobby.paperCore.getLanguageHandler();
        this.itemHandler = this.lobby.paperCore.getItemHandler();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());

        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.GadgetMenue(contents, playerObject);
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
}
