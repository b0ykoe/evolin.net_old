package net.evolin.papercore.gadgets.gadgets;

import net.evolin.papercore.PaperCore;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;


@SuppressWarnings("FieldCanBeLocal")
public class ParticleGadget extends GadgetStates {

    @SuppressWarnings("unused")
    private BukkitTask task;
    private Particle particle;
    private PaperCore paperCore;
    private PlayerObject playerObject;
    private Player player;

    @Override
    public void start(PaperCore paperCore, PlayerObject playerObject) {
        this.paperCore = paperCore;
        this.playerObject = playerObject;
        this.player = this.playerObject.getPlayer();
        this.asyncTasks = true;
    }

    @Override
    public void runAsync() {
        if (particle != null)
            switch (particle) {
                case FLAME:
                    spawnParticles(2, 0, 0, 0, 0.1, null, false);
                    break;
                case WATER_SPLASH:
                    spawnParticles(5, 0.5, 0, 0.5, 0, null, false);
                    break;
                case PORTAL:
                    spawnParticles(3);
                    break;
            }
    }

    /**
     * spawns set particles
     *
     * @param count int - count of particles
     */
    @SuppressWarnings("SameParameterValue")
    private void spawnParticles(int count) {
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(this.player.getUniqueId());
        Bukkit.getOnlinePlayers().forEach(onlinePlayer -> {
            PlayerObject otherPlayerObject = this.paperCore.getUuidPlayerObject(onlinePlayer.getUniqueId());
            if (otherPlayerObject.canSee(playerObject))
                onlinePlayer.spawnParticle(this.particle, this.player.getLocation(), count);
        });
    }

    /**
     * spawns particles at a given location
     *
     * @param count   int - count of particles
     * @param offsetX double -
     * @param offsetY double -
     * @param offsetZ double -
     * @param extra   double -
     * @param data    Object -
     * @param force   boolean -
     */
    @SuppressWarnings("SameParameterValue")
    private void spawnParticles(int count, double offsetX, double offsetY, double offsetZ, double extra, Object data, boolean force) {
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(this.player.getUniqueId());
        Bukkit.getOnlinePlayers().forEach(onlinePlayer -> {
            PlayerObject otherPlayerObject = this.paperCore.getUuidPlayerObject(onlinePlayer.getUniqueId());
            if (otherPlayerObject.canSee(playerObject))
                onlinePlayer.spawnParticle(this.particle, this.player.getLocation(), count, offsetX, offsetY, offsetZ, extra, data);
        });
    }

    /**
     * Sets particles onto a itemstack
     *
     * @param item     itemstack -
     * @param particle particle -
     */
    public void setParticle(ItemStack item, Particle particle) {
        this.selectedItem = item;
        this.particle = particle;
    }

    @Override
    public void stop() {
        particle = null;
    }
}

