package net.evolin.discord.jLine;

import ch.qos.logback.classic.pattern.ThrowableProxyConverter;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.core.CoreConstants;
import ch.qos.logback.core.LayoutBase;
import ch.qos.logback.core.util.CachingDateFormatter;

public class JLineLayout extends LayoutBase<ILoggingEvent> {

    // Layout for the Console OUTPUT
    // dd.MM HH:mm:ss [LEVEL] - MSG \n
    // 01.01 13:02:59 [INFO] - This is a test \n
    private CachingDateFormatter cachingDateFormatter = new CachingDateFormatter("dd.MM HH:mm:ss");
    private ThrowableProxyConverter tpc = new ThrowableProxyConverter();

    /**
     * builds and returns the current layout
     *
     * @param event event triggered on logging
     * @return string of the current layout
     */
    public String doLayout(ILoggingEvent event) {
        if (!isStarted()) {
            return CoreConstants.EMPTY_STRING;
        }
        StringBuilder sb = new StringBuilder();

        long timestamp = event.getTimeStamp();

        sb.append(this.cachingDateFormatter.format(timestamp));
        sb.append(" ");
        sb.append("[");
        // Colors for certain events
        switch (event.getLevel().toString().toLowerCase()) {
            case "error":
                sb.append(JLineColors.ANSI_RED);
                break;
            case "info":
                sb.append(JLineColors.ANSI_WHITE);
                break;
            case "warn":
                sb.append(JLineColors.ANSI_YELLOW);
                break;
            case "debug":
                sb.append(JLineColors.ANSI_BLUE);
                break;
            default:
                break;
        }
        sb.append(event.getLevel().toString());
        sb.append(JLineColors.ANSI_RESET);
        sb.append("]");
        // if WARN or INFO add a additional space
        if (event.getLevel().toString().length() == 4)
            sb.append(" ");
        sb.append(" - ");
        sb.append(event.getFormattedMessage());
        sb.append(CoreConstants.LINE_SEPARATOR);
        IThrowableProxy tp = event.getThrowableProxy();
        if (tp != null) {
            String stackTrace = this.tpc.convert(event);
            sb.append(stackTrace);
        }
        return sb.toString();
    }
}