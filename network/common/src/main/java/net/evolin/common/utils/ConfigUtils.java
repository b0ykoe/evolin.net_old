package net.evolin.common.utils;

import org.bukkit.Bukkit;
import org.bukkit.Location;

public class ConfigUtils {

    /**
     * transforms string into a location
     * Template world,x,y,z,pitch,yaw
     *
     * @param locationString Template world,x,y,z,pitch,yaw
     * @return returns a location
     */
    public static Location locationFromString(String locationString) {
        String[] args = locationString.split(",");
        double[] parsed = new double[5];
        for (int a = 0; a < 3; a++) {
            parsed[a] = Double.parseDouble(args[a + 1]);
        }

        return new Location(Bukkit.getWorld(args[0]), parsed[0], parsed[1], parsed[2], Float.parseFloat(args[4]), Float.parseFloat(args[5]));
    }

    /**
     * transforms a location to a string
     *
     * @param location location which has to be serialized
     * @return returns a string world,x,y,z,pitch,yaw
     */
    public static String locationToString(Location location) {
        String locationString = "";
        locationString += location.getWorld().getName() + ",";
        locationString += location.getX() + ",";
        locationString += location.getY() + ",";
        locationString += location.getZ() + ",";
        locationString += location.getYaw() + ",";
        locationString += location.getPitch();
        return locationString;
    }
}
