package net.evolin.discord.commands.commands;

import net.evolin.discord.Discord;
import net.evolin.discord.commands.SubCommand;
import net.evolin.discord.jLine.JLineColors;

import java.util.List;

public class HelpCommand extends SubCommand {

    private List<SubCommand> subCommands;

    /**
     * adds the exit command to the commandhandler
     *
     * @param discord discord instance
     * @param subCommands list of subcommands to show the helppage properly
     */
    public HelpCommand(Discord discord, List<SubCommand> subCommands) {
        super(discord, "help", "help", discord.languageHandler.get("help-command-description", "Shows this help page."));
        this.getAliases().add("hilfe");
        this.subCommands = subCommands;
    }

    /**
     * executes the help command
     *
     * @param args stringlist of arguments (there none for this command)
     */
    @Override
    public void execute(String... args) {
        this.subCommands.forEach(subCommand -> {
            String output = this.discord.languageHandler.get("help-list-layout", "%CYAN%%command% %RESET%(%CYAN%%aliases%%RESET%) | %description%");
            output = JLineColors.replace(output);
            output = output.replace("%command%", subCommand.getSyntax())
                    .replace("%aliases%", subCommand.getAliasesString())
                    .replace("%description%", subCommand.getDescription());
            discord.getLogger().info(output);
        });
    }
}
