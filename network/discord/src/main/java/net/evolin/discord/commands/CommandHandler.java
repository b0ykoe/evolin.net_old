package net.evolin.discord.commands;

import net.evolin.discord.Discord;
import net.evolin.discord.commands.commands.ExitCommand;
import net.evolin.discord.commands.commands.HelpCommand;
import net.evolin.discord.commands.commands.ReloadCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class CommandHandler {

    @SuppressWarnings("FieldCanBeLocal")
    private Discord discord;
    private List<SubCommand> subCommands;
    private SubCommand helpCommand;

    /**
     * super of the commandhandler, registering all commands here
     *
     * @param discord discord instance
     */
    public CommandHandler(Discord discord) {
        this.discord = discord;
        this.subCommands = new ArrayList<>();
        this.subCommands.add(new ReloadCommand(discord));
        this.subCommands.add(new ExitCommand(discord));
        this.subCommands.add(this.helpCommand = new HelpCommand(this.discord, this.subCommands));
    }

    /**
     * method to find and execute a command
     *
     * @param args stringlist - args (name, args...)
     */
    public void handle(String... args) {
        if (args.length == 0) {
            this.helpCommand.execute();
            return;
        }
        // Find command with given name
        Optional<SubCommand> commandOptional = this.subCommands.stream().filter(command -> command.getName().equals(args[0]) || command.getAliases().contains(args[0].toLowerCase())).findFirst();
        // Remove first element of array and execute
        commandOptional.ifPresent(subCommand -> subCommand.execute(Arrays.copyOfRange(args, 1, args.length)));

    }
}
