package net.evolin.discord.commands.commands;


import net.evolin.discord.Discord;
import net.evolin.discord.commands.SubCommand;

public class ReloadCommand extends SubCommand {
    /**
     * adds the exit command to the commandhandler
     *
     * @param discord discord instance
     */
    public ReloadCommand(Discord discord) {
        super(discord, "reload", "reload", discord.languageHandler.get("reload-command-description", "Reloads all config files."));
        this.getAliases().add("rl");
    }

    /**
     * executes the reload command
     *
     * @param args stringlist of arguments (there none for this command)
     */
    @Override
    public void execute(String... args) {
        this.discord.init();
        this.discord.getLogger().info(this.discord.languageHandler.get("reload-command-sucessfully", "Reloaded and applied all config files."));
    }
}
