package net.evolin.papercore;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceId;
import de.dytanic.cloudnet.wrapper.Wrapper;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.papercore.commands.SettingsCommand;
import net.evolin.papercore.handler.InventoryHandler;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.listener.ChannelListener;
import net.evolin.papercore.listener.ConfigListener;
import net.evolin.papercore.listener.EntityHider;
import net.evolin.papercore.listener.PlayerListener;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.HashMap;
import java.util.UUID;

public final class PaperCore extends JavaPlugin {

    private ServiceId serviceId = Wrapper.getInstance().getServiceId();
    private LanguageHandler languageHandler = null;
    private ConfigHandler configHandler = null;
    private InventoryHandler inventoryHandler = null;
    private ItemHandler itemHandler = null;
    private static PaperCore paperCore;
    private HashMap<UUID, PlayerObject> uuidPlayerObjectHashMap = new HashMap<>();
    private EntityHider entityHider;


    @Override
    public void onEnable() {
        // Plugin startup logic
        paperCore = this;

        // registers extra classes
        this.entityHider = new EntityHider(this, EntityHider.Policy.BLACKLIST);

        // register commands
        this.getCommand("settings").setExecutor(new SettingsCommand(this));

        // registers all normal PlayerListeners (tab and so on)
        Listener playerListener = new PlayerListener(this);
        this.getServer().getPluginManager().registerEvents(playerListener, this);
        CloudNetDriver.getInstance().getEventManager().registerListener(playerListener);

        // register for CloudNet config Event
        CloudNetDriver.getInstance().getEventManager().registerListener(new ConfigListener(this));
        CloudNetDriver.getInstance().getEventManager().registerListener(new ChannelListener(this));

        // calls a sendChannelMessage to request the current configuration after start
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("requestConfig", String.valueOf(this.serviceId.getUniqueId()), new JsonDocument());
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("serverStartChannel", this.serviceId.getTaskName() , new JsonDocument()
                .append("serviceName", this.serviceId.getName()));
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        CloudNetDriver.getInstance().getEventManager().unregisterListeners(this.getClass().getClassLoader());
        Wrapper.getInstance().unregisterPacketListenersByClassLoader(this.getClass().getClassLoader());
    }

    public EntityHider getEntityHider() {
        return entityHider;
    }

    public ServiceId getServiceId() {
        return serviceId;
    }

    public LanguageHandler getLanguageHandler() {
        return languageHandler;
    }

    public void setLanguageHandler(LanguageHandler languageHandler) {
        this.languageHandler = languageHandler;
    }

    public ConfigHandler getConfigHandler() {
        return configHandler;
    }

    public void setConfigHandler(ConfigHandler configHandler) {
        this.configHandler = configHandler;
    }

    public InventoryHandler getInventoryHandler() {
        return inventoryHandler;
    }

    public void setInventoryHandler(InventoryHandler inventoryHandler) {
        this.inventoryHandler = inventoryHandler;
    }

    public ItemHandler getItemHandler() {
        return itemHandler;
    }

    public void setItemHandler(ItemHandler itemHandler) {
        this.itemHandler = itemHandler;
    }

    public HashMap<UUID, PlayerObject> getUuidPlayerMap() {
        return uuidPlayerObjectHashMap;
    }

    public PlayerObject getUuidPlayerObject(UUID uuid) {
        return uuidPlayerObjectHashMap.get(uuid);
    }

    public void addUuidPlayerObject(UUID uuid,PlayerObject playerObject) {
        this.uuidPlayerObjectHashMap.put(uuid, playerObject);
    }

    public void removeUuidPlayerObject(UUID uuid) {
        this.uuidPlayerObjectHashMap.remove(uuid);
    }

    /**
     *
     * @return returns the paper instance
     */
    public static PaperCore getInstance() {
        return paperCore;
    }
}
