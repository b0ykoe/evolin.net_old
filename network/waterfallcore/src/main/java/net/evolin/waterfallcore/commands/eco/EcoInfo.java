package net.evolin.waterfallcore.commands.eco;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.waterfallcore.WaterfallCore;
import net.evolin.waterfallcore.commands.SubCommand;
import net.evolin.common.objects.EcoObject;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class EcoInfo extends SubCommand {
    public EcoInfo(WaterfallCore waterfallCore) {
        super(waterfallCore, "info","info", "Zeigt money an");
    }

    @Override
    public void execute(ProxiedPlayer player, String[] args) {
        ProxiedPlayer receiver = null;
        ICloudPlayer iCloudPlayer = null;

        if(!this.hasPermission(player))
            return;

        if(args.length == 0)
            receiver = player;

        if(args.length == 1)
            receiver = ProxyServer.getInstance().getPlayer(args[0]);

        if(receiver == null) {
            player.sendMessage("Der Spieler %s ist nicht online".replace("%s", args[0]));
            return;
        }

        iCloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(receiver.getUniqueId());
        if(!iCloudPlayer.getName().equals(player.getName())) {
            player.sendMessage("%s hat %i Coins".replace("%s", args[0]).replace("%i", String.valueOf(EcoObject.getCoins(iCloudPlayer))));
        } else
            player.sendMessage("Du hast %i Coins".replace("%i", String.valueOf(EcoObject.getCoins(iCloudPlayer))));
    }
}
