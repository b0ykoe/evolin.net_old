package net.evolin.lobby;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.wrapper.Wrapper;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.lobby.commands.StopCommand;
import net.evolin.lobby.inventories.HideInventory;
import net.evolin.lobby.inventories.LobbySwitcherInventory;
import net.evolin.lobby.inventories.SilentLobbySwitcherInventory;
import net.evolin.lobby.inventories.TeleporterInventory;
import net.evolin.lobby.inventories.gadgets.BalloonInventory;
import net.evolin.lobby.inventories.gadgets.GadgetInventory;
import net.evolin.lobby.inventories.gadgets.ParticleInventory;
import net.evolin.lobby.inventories.gadgets.ShoeInventory;
import net.evolin.lobby.listeners.CancelListener;
import net.evolin.lobby.listeners.CloudListener;
import net.evolin.lobby.listeners.HotbarListener;
import net.evolin.lobby.listeners.LobbyListener;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.handler.InventoryHandler;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.objects.ItemObject;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.Map;
import java.util.Objects;

public final class Lobby extends JavaPlugin {

    public final String serviceName = Wrapper.getInstance().getServiceId().getName();
    public boolean shutdown = false;
    public boolean shutdownTimer = false;
    public PaperCore paperCore;

    @Override
    public void onEnable() {
        // Plugin startup logic
        // get PaperCore Instance
        this.paperCore = PaperCore.getInstance();

        // register commands
        Objects.requireNonNull(this.getCommand("exit")).setExecutor(new StopCommand(this));

        // register listeners
        this.getServer().getPluginManager().registerEvents(new CancelListener(this), this);
        this.getServer().getPluginManager().registerEvents(new LobbyListener(this), this);
        this.getServer().getPluginManager().registerEvents(new HotbarListener(this), this);

        Listener cloudListener = new CloudListener(this);
        getServer().getPluginManager().registerEvents(cloudListener, this);
        CloudNetDriver.getInstance().getEventManager().registerListener(cloudListener);

        // changes time on all worlds
        Bukkit.getServer().getScheduler().scheduleSyncRepeatingTask(this, () -> {
            for (World w : Bukkit.getServer().getWorlds()) {
                w.setTime(0L);
            }
        }, 0L, 1000L);

        for (World w : Bukkit.getServer().getWorlds()) {
            for(Entity e : w.getEntities()) {
                e.remove();
            }
        }

        for (Map.Entry<String, Map<String, Map<String, String>>> entry : this.paperCore.getLanguageHandler().languages.entrySet()) {
            init(entry.getKey());
        }

        // GadgetManager
        Bukkit.getScheduler().runTaskTimer(this, ()->this.paperCore.getUuidPlayerMap().forEach((uuid, playerObject) -> playerObject.getGadgetManager().run()), 1L, 1L);
        Bukkit.getScheduler().runTaskTimerAsynchronously(this, ()->this.paperCore.getUuidPlayerMap().forEach((uuid, playerObject) -> playerObject.getGadgetManager().runAsync()), 1L, 1L);
    }


    public void init(String language) {
        ItemHandler itemHandler = this.paperCore.getItemHandler();
        InventoryHandler inventoryHandler = this.paperCore.getInventoryHandler();
        LanguageHandler languageHandler = this.paperCore.getLanguageHandler();

        itemHandler.addItem(language, "itemSpawn", new ItemObject(Material.ARMOR_STAND).setDisplayName(languageHandler.get(language, "itemSpawn", "§cSpawn")));
        itemHandler.addItem(language, "itemGame1", new ItemObject(Material.YELLOW_BED).setDisplayName(languageHandler.get(language, "itemGame1", "§cGame1")));
        itemHandler.addItem(language, "itemGame2", new ItemObject(Material.DIAMOND_BOOTS).setDisplayName(languageHandler.get(language, "itemGame2", "§cGame2")));


        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.LIME_STAINED_GLASS));
        itemHandler.addItem(language, "currentServer", new ItemObject(Material.WHITE_STAINED_GLASS));

        // teleporter
        inventoryHandler.addInventory(language, "inventoryTeleporter", inventoryHandler.createInventory("inventoryTeleporter", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventoryTeleporterTitle", "§cTeleporter"), true, new TeleporterInventory(this)));
        inventoryHandler.addInventory(language, "inventorySilentLobbySwitcher", inventoryHandler.createInventory("inventorySilentLobbySwitcher", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventorySilentLobbySwitcherTitle", "§cSilent-Lobby-Switcher"), true, new SilentLobbySwitcherInventory(this)));
        inventoryHandler.addInventory(language, "inventoryLobbySwitcher", inventoryHandler.createInventory("inventoryLobbySwitcher", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventoryLobbySwitcherTitle", "§cLobby-Switcher"), true, new LobbySwitcherInventory(this)));

        itemHandler.addItem(language, "itemTeleporter", new ItemObject(Material.COMPASS).setDisplayName(languageHandler.get(language, "itemTeleporter", "§cTeleporter")));
        itemHandler.addItem(language, "itemSilentLobbySwitcher", new ItemObject(Material.TNT).setDisplayName(languageHandler.get(language, "itemSilentLobbySwitcher", "§cSilent-Lobby-Switcher")));
        itemHandler.addItem(language, "itemLobbySwitcher", new ItemObject(Material.NETHER_STAR).setDisplayName(languageHandler.get(language, "itemLobbySwitcher", "§cLobby-Switcher")));

        // gadgets
        inventoryHandler.addInventory(language, "inventoryGadgets", inventoryHandler.createInventory("inventoryGadgets", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventoryGadgetsTitle", "§cGadgets"), true, new GadgetInventory(this)));
        inventoryHandler.addInventory(language, "inventoryParticles", inventoryHandler.createInventory("inventoryParticles", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventoryParticlesTitle", "§cParticles"), true, new ParticleInventory(this)));
        inventoryHandler.addInventory(language, "inventoryShoes", inventoryHandler.createInventory("inventoryShoes", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventoryShoesTitle", "§cShoes"), true, new ShoeInventory(this)));
        inventoryHandler.addInventory(language, "inventoryBalloons", inventoryHandler.createInventory("inventoryBalloons", 5, 9, InventoryType.CHEST, languageHandler.get(language, "inventoryBalloonsTitle", "§cBalloons"), true, new BalloonInventory(this)));

        itemHandler.addItem(language, "itemRemoveShoes", new ItemObject(Material.BARRIER).setDisplayName(languageHandler.get(language, "itemRemoveShoes", "§cRemove Shoes")));
        itemHandler.addItem(language, "itemRemoveParticles", new ItemObject(Material.BARRIER).setDisplayName(languageHandler.get(language, "itemRemoveParticles", "§cRemove Particle")));
        itemHandler.addItem(language, "itemRemoveBalloons", new ItemObject(Material.BARRIER).setDisplayName(languageHandler.get(language, "itemRemoveBalloons", "§cRemove Balloon")));
        itemHandler.addItem(language, "itemGadgets", new ItemObject(Material.FIREWORK_ROCKET).setDisplayName(languageHandler.get(language, "itemGadgets", "§cGadgets")).setLore("§cDies ist ein Test \n Zweite Reihe"));
        itemHandler.addItem(language, "itemBalloons", new ItemObject(Material.RED_WOOL).setDisplayName(languageHandler.get(language, "itemBalloons", "§cBalloons")));
        itemHandler.addItem(language, "itemParticles", new ItemObject(Material.BLAZE_POWDER).setDisplayName(languageHandler.get(language, "itemParticles", "§cParticles")));
        itemHandler.addItem(language, "itemShoes", new ItemObject(Material.LEATHER_BOOTS).setDisplayName(languageHandler.get(language, "itemShoes", "§cShoes")));

        // Balloons
        itemHandler.addItem(language, "itemBalloonGreen", new ItemObject(Material.GREEN_TERRACOTTA).setDisplayName(languageHandler.get(language, "itemBalloonGreen", "§cGreen")));
        itemHandler.addItem(language, "itemBalloonBlue", new ItemObject(Material.BLUE_TERRACOTTA).setDisplayName(languageHandler.get(language, "itemBalloonBlue", "§cBlue")));
        itemHandler.addItem(language, "itemBalloonOrange", new ItemObject(Material.ORANGE_TERRACOTTA).setDisplayName(languageHandler.get(language, "itemBalloonOrange", "§cOrange")));
        itemHandler.addItem(language, "itemBalloonYellow", new ItemObject(Material.YELLOW_TERRACOTTA).setDisplayName(languageHandler.get(language, "itemBalloonYellow", "§cYellow")));
        itemHandler.addItem(language, "itemBalloonRed", new ItemObject(Material.RED_TERRACOTTA).setDisplayName(languageHandler.get(language, "itemBalloonRed", "§cRed")));

        // shoes
        itemHandler.addItem(language, "itemAquaShoes", new ItemObject(Material.LEATHER_BOOTS).setArmorColor(Color.AQUA).setDisplayName(languageHandler.get(language, "itemAquaShoes", "§cFast Boots")));
        itemHandler.addItem(language, "itemGrayShoes", new ItemObject(Material.LEATHER_BOOTS).setArmorColor(Color.GRAY).setDisplayName(languageHandler.get(language, "itemGrayShoes", "§cSlow Boots")));
        itemHandler.addItem(language, "itemOrangeShoes", new ItemObject(Material.LEATHER_BOOTS).setArmorColor(Color.ORANGE).setDisplayName(languageHandler.get(language, "itemOrangeShoes", "§cJump Boots")));

        // particles
        itemHandler.addItem(language, "itemBlazeParticles", new ItemObject(Material.BLAZE_POWDER).setDisplayName(languageHandler.get(language, "itemBlazeParticles", "§cFire Particles")));
        itemHandler.addItem(language, "itemLapisParticles", new ItemObject(Material.LAPIS_LAZULI).setDisplayName(languageHandler.get(language, "itemLapisParticles", "§cWater Particles")));
        itemHandler.addItem(language, "itemEnderParticles", new ItemObject(Material.ENDER_EYE).setDisplayName(languageHandler.get(language, "itemEnderParticles", "§cEnder Particles")));

        // hider
        inventoryHandler.addInventory(language, "inventoryHide", inventoryHandler.createInventory("inventoryHide", 1, 5, InventoryType.BREWING, languageHandler.get(language, "inventoryLobbySwitcherTitle", "§cLobby-Switcher"), true, new HideInventory(this)));
        itemHandler.addItem(language, "itemHide", new ItemObject(Material.BLAZE_ROD).setDisplayName(languageHandler.get(language, "itemHide", "§cPlayer-Hider")));

        // hide
        itemHandler.addItem(language, "ITEM_gray_none", new ItemObject(Material.GRAY_DYE)
                .setDisplayName(languageHandler.get(language, "ITEM_gray_none_TITLE", "§6Nobody"))
                .setLore(languageHandler.get(language, "ITEM_gray_none_LORE", "§6Hides all players.")));
        itemHandler.addItem(language, "ITEM_lila_vip", new ItemObject(Material.PURPLE_DYE)
                .setDisplayName(languageHandler.get(language, "ITEM_lila_vip_TITLE", "§6YouTuber and Teammember"))
                .setLore(languageHandler.get(language, "ITEM_lila_vip_LORE", "§6Shows all teammembers and youtubers.")));
        itemHandler.addItem(language, "ITEM_green_all", new ItemObject(Material.LIME_DYE)
                .setDisplayName(languageHandler.get(language, "ITEM_green_all_TITLE", "§6Everyone"))
                .setLore(languageHandler.get(language, "ITEM_green_all_LORE", "§6Shows all players")));

        itemHandler.addItem(language, "ITEM_gray_none_enchanted", new ItemObject(new ItemObject(Material.GRAY_DYE).hideEnchantments())
                .setDisplayName(languageHandler.get(language, "ITEM_gray_none_TITLE", "§6Nobody"))
                .setLore(languageHandler.get(language, "ITEM_gray_none_LORE", "§6Hides all players.")));
        itemHandler.addItem(language, "ITEM_lila_vip_enchanted", new ItemObject(new ItemObject(Material.PURPLE_DYE).hideEnchantments())
                .setDisplayName(languageHandler.get(language, "ITEM_lila_vip_TITLE", "§6YouTuber and Teammember"))
                .setLore(languageHandler.get(language, "ITEM_lila_vip_LORE", "§6Shows all teammembers and youtubers.")));
        itemHandler.addItem(language, "ITEM_green_all_enchanted", new ItemObject(new ItemObject(Material.LIME_DYE).hideEnchantments())
                .setDisplayName(languageHandler.get(language, "ITEM_green_all_TITLE", "§6Everyone"))
                .setLore(languageHandler.get(language, "ITEM_green_all_LORE", "§6Shows all players")));


        /*itemHandler.addItem(language, "itemLobbySwitcher", new ItemObject(Material.NETHER_STAR));
        itemHandler.addItem(language, "itemGadgets", new ItemObject(Material.FIREWORK_ROCKET));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.POPPY));

        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.BLAZE_ROD));

        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.TNT));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.NETHER_STAR));

        // compass
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.GRAY_STAINED_GLASS_PANE));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.ARMOR_STAND));

        // hide
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.GRAY_DYE));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.PURPLE_DYE));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.LIME_DYE));

        // gadgets
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.LEATHER_BOOTS));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.BLAZE_POWDER));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.RED_WOOL));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.BARRIER));

        // balloons
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.GREEN_TERRACOTTA));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.BLUE_TERRACOTTA));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.ORANGE_TERRACOTTA));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.YELLOW_TERRACOTTA));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.RED_TERRACOTTA));

        // shoes
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.LEATHER_BOOTS).setArmorColor(Color.AQUA));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.LEATHER_BOOTS).setArmorColor(Color.GRAY));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.LEATHER_BOOTS).setArmorColor(Color.ORANGE));

        // particles
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.BLAZE_POWDER));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.LAPIS_LAZULI));
        itemHandler.addItem(language, "onlineServer", new ItemObject(Material.ENDER_EYE));*/
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        CloudNetDriver.getInstance().getEventManager().unregisterListeners(this.getClass().getClassLoader());
        Wrapper.getInstance().unregisterPacketListenersByClassLoader(this.getClass().getClassLoader());
    }
}
