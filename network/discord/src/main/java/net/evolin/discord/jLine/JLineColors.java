package net.evolin.discord.jLine;

@SuppressWarnings("all")
public class JLineColors {

    // Colors for terminals
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_BLACK = "\u001B[30m";
    public static final String ANSI_RED = "\u001B[31m";
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_YELLOW = "\u001B[33m";
    public static final String ANSI_BLUE = "\u001B[34m";
    public static final String ANSI_PURPLE = "\u001B[35m";
    public static final String ANSI_CYAN = "\u001B[36m";
    public static final String ANSI_WHITE = "\u001B[37m";

    public static final String ANSI_BLACK_BACKGROUND = "\u001B[40m";
    public static final String ANSI_RED_BACKGROUND = "\u001B[41m";
    public static final String ANSI_GREEN_BACKGROUND = "\u001B[42m";
    public static final String ANSI_YELLOW_BACKGROUND = "\u001B[43m";
    public static final String ANSI_BLUE_BACKGROUND = "\u001B[44m";
    public static final String ANSI_PURPLE_BACKGROUND = "\u001B[45m";
    public static final String ANSI_CYAN_BACKGROUND = "\u001B[46m";
    public static final String ANSI_WHITE_BACKGROUND = "\u001B[47m";

    public static String replace(String string) {
        return string.replace("%RESET%", ANSI_RESET)
                .replace("%BLACK%", ANSI_BLACK)
                .replace("%RED%", ANSI_RED)
                .replace("%GREEN%", ANSI_GREEN)
                .replace("%YELLOW%", ANSI_YELLOW)
                .replace("%BLUE%", ANSI_BLUE)
                .replace("%PURPLE%", ANSI_PURPLE)
                .replace("%CYAN%", ANSI_CYAN)
                .replace("%WHITE%", ANSI_WHITE)
                .replace("%BG_BLACK%", ANSI_BLACK_BACKGROUND)
                .replace("%BG_RED%", ANSI_RED_BACKGROUND)
                .replace("%BG_GREEN%", ANSI_GREEN_BACKGROUND)
                .replace("%BG_YELLOW%", ANSI_YELLOW_BACKGROUND)
                .replace("%BG_BLUE%", ANSI_BLUE_BACKGROUND)
                .replace("%BG_PURPLE%", ANSI_PURPLE_BACKGROUND)
                .replace("%BG_CYAN%", ANSI_CYAN_BACKGROUND)
                .replace("%BG_WHITE%", ANSI_WHITE_BACKGROUND);
    }
}
