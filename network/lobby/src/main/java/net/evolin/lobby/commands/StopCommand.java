package net.evolin.lobby.commands;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.BridgeHelper;
import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.bukkit.BukkitCloudNetHelper;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import de.dytanic.cloudnet.ext.bridge.player.IPlayerManager;
import net.evolin.common.utils.CloudUtils;
import net.evolin.common.utils.ServerUtils;
import net.evolin.lobby.Lobby;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.Collection;
import java.util.Random;

public class StopCommand implements CommandExecutor {

    private Lobby lobby;

    public StopCommand(Lobby lobby) {
        this.lobby = lobby;
    }


    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        IPlayerManager bridgePlayerManager = BridgePlayerManager.getInstance();
        ICloudPlayer cloudPlayer = bridgePlayerManager.getOnlinePlayer(player.getUniqueId());

        BukkitCloudNetHelper.setMaxPlayers(0);
        BridgeHelper.updateServiceInfo();

        ServerUtils.sendAllMessage(this.lobby.getServer().getOnlinePlayers(), this.lobby.paperCore.getLanguageHandler().get("command-server-exit-players", "The Server %server% is shutting down. You'll be teleported to another one..").replace("%server%", this.lobby.serviceName));
        if (CloudUtils.countServiceInfoSnapshotCount("Lobby") <= 1) {
            CloudUtils.createStartSerivce("Lobby");
            sender.sendMessage(this.lobby.paperCore.getLanguageHandler().get("command-server-exit", "The server will soon shutdown.."));
            lobby.shutdown = true;
        } else {
            Collection<ServiceInfoSnapshot> cloudServices = CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices("Lobby");
            Random r = new Random();
            String task = cloudServices.stream()
                    .filter(v -> !(this.lobby.serviceName.equals(v.getServiceId().getName())))
                    .skip(r.nextInt(cloudServices.size()-1))
                    .findFirst()
                    .get()
                    .getServiceId()
                    .getName();
            ServerUtils.safeSwapProperties(this.lobby.getServer().getOnlinePlayers(), task, "forceSwap");
        }

        return true;
    }
}