package net.evolin.papercore.handler;

import net.evolin.common.handler.LanguageHandler;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.ItemObject;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;
import java.util.Map;

public class ItemHandler {

    // 1. HashMap <String, HashMap> - LanguageName <HashMap> - ex. English - <HashMap>
    // 2. HashMap <String, ItemObject> - InventoryName Item - ex. ConfigInventory ItemObject
    private HashMap<String, HashMap<String, ItemObject>> itemMap = new HashMap<>();
    private LanguageHandler languageHandler;
    private String defaultLanguage;
    private PaperCore paperCore;

    public ItemHandler(PaperCore paperCore) {
        this.paperCore = paperCore;
        this.languageHandler = this.paperCore.getLanguageHandler();
        this.defaultLanguage = this.languageHandler.defaultLanguage;

        for(Map.Entry<String, Map<String, Map<String, String>>> entry : this.languageHandler.languages.entrySet()) {
            init(entry.getKey());
        }
    }

    /**
     *
     * @param language
     */
    private void init(String language) {
        this.addItem(language, "itemOn", new ItemObject(new ItemStack(Material.GREEN_WOOL)).setDisplayName("§cTrue"));
        this.addItem(language, "itemOff", new ItemObject(new ItemStack(Material.RED_WOOL)).setDisplayName("§cFalse"));

        this.addItem(language, "emptyItem", new ItemObject(new ItemStack(Material.GRAY_STAINED_GLASS_PANE)).setDisplayName("§c"));
        this.addItem(language, "itemSettings", new ItemObject(Material.REPEATER).setDisplayName(this.languageHandler.get(language, "itemSettingsDisplay", "§cSettings")));

        // Lobby
        this.addItem(language, "itemLobbySettings", new ItemObject(Material.ARMOR_STAND).setDisplayName(this.languageHandler.get(language, "itemLobbySettings", "§cLobby Settings")));
        this.addItem(language, "itemLobbySettingsSafePosition", new ItemObject(Material.BOOK).setDisplayName(this.languageHandler.get(language, "itemLobbySettingsSafePosition", "§cSafe Lobby Position")));

        // Language
        this.addItem(language, "itemLanguage", new ItemObject(Material.PAPER).setDisplayName(this.languageHandler.get(language, "itemLanguageDisplay", "§cLanguages")));
        String germanHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvNWU3ODk5YjQ4MDY4NTg2OTdlMjgzZjA4NGQ5MTczZmU0ODc4ODY0NTM3NzQ2MjZiMjRiZDhjZmVjYzc3YjNmIn19fQ==";
        this.addItem(language, "itemLanguageGerman", new ItemObject(Material.PLAYER_HEAD).setHeadTexture(germanHead).setDisplayName(this.languageHandler.get(language, "itemLanguageGermanDisplay", "§cDeutsch")));
        String englishHead = "eyJ0ZXh0dXJlcyI6eyJTS0lOIjp7InVybCI6Imh0dHA6Ly90ZXh0dXJlcy5taW5lY3JhZnQubmV0L3RleHR1cmUvYmVlNWM4NTBhZmJiN2Q4ODQzMjY1YTE0NjIxMWFjOWM2MTVmNzMzZGNjNWE4ZTIxOTBlNWMyNDdkZWEzMiJ9fX0=";
        this.addItem(language, "itemLanguageEnglish", new ItemObject(Material.PLAYER_HEAD).setHeadTexture(englishHead).setDisplayName(this.languageHandler.get(language, "itemLanguageEnglishDisplay", "§cEnglish")));
    }

    /**
     * adds a item to the HashMap
     * @param language String - language of the item
     * @param itemName String - name of the item
     * @param itemObject ItemObject - item
     */
    public void addItem(String language, String itemName, ItemObject itemObject) {
        // if HashMap does not contain language (usually happens on the first inventories)
        if(!this.itemMap.containsKey(language))
            this.itemMap.put(language, new HashMap<>());
        this.itemMap.get(language).put(itemName, itemObject);
    }

    /**
     * returns the inventory requested from lang
     * @param language language of the inventory
     * @param name internal name of the inventory
     * @return null or the item or the fallback item (if the language does not have the item)
     */
    public ItemObject getItem(String language, String name) {
        // if language exist in map
        if(this.itemMap.containsKey(language))
            // if language does contain the inventory
            if(this.itemMap.get(language).containsKey(name))
                return this.itemMap.get(language).get(name);
        // if default language does contain the inventory return
        if(this.itemMap.get(this.defaultLanguage).containsKey(name))
            return getItem(this.defaultLanguage, name);
        // else return nothing
        return null;
    }

}
