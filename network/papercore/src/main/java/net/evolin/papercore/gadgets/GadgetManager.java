package net.evolin.papercore.gadgets;

import net.evolin.papercore.PaperCore;
import net.evolin.papercore.gadgets.gadgets.BalloonGadget;
import net.evolin.papercore.gadgets.gadgets.ParticleGadget;
import net.evolin.papercore.gadgets.gadgets.ShoeGadget;
import net.evolin.papercore.objects.PlayerObject;

import java.util.ArrayList;
import java.util.List;

public class GadgetManager {
    private PaperCore paperCore;
    private PlayerObject playerObject;
    private GadgetStates[] states;
    private List<GadgetStates> currentStates;

    /**
     *
     * @param paperCore
     * @param playerObject
     */
    public GadgetManager(PaperCore paperCore, PlayerObject playerObject) {
        this.paperCore = paperCore;
        this.playerObject = playerObject;
        states = new GadgetStates[3];

        states[GadgetStates.BALLOONS] = new BalloonGadget();
        states[GadgetStates.SHOES] = new ShoeGadget();
        states[GadgetStates.PARTICLES] = new ParticleGadget();

        this.currentStates = new ArrayList<>();
    }

    /**
     *
     */
    public void run() {
        this.currentStates.forEach(GadgetStates::run);
    }

    /**
     *
     */
    public void runAsync() {
        this.currentStates.forEach(currentState -> {
            if (currentState.hasAsyncTasks()) currentState.runAsync();
        });
    }

    /**
     *
     * @param stateID
     */
    public void setState(int stateID) {
        if (hasState(stateID)) return;
        GadgetStates state = states[stateID];
        state.start(this.paperCore, this.playerObject);
        this.currentStates.add(state);
    }

    /**
     *
     */
    public void stopCurrentStates() {
        stopCurrentState(GadgetStates.BALLOONS);
        stopCurrentState(GadgetStates.SHOES);
        stopCurrentState(GadgetStates.PARTICLES);
    }

    /**
     *
     * @param stateID
     */
    public void stopCurrentState(int stateID) {
        if (!hasState(stateID)) return;
        GadgetStates state = states[stateID];
        state.stop();
        this.currentStates.remove(state);
    }

    /**
     *
     * @return
     */
    public List<GadgetStates> getCurrentStates() {
        return currentStates;
    }

    /**
     *
     * @param stateID
     * @return
     */
    public boolean hasState(int stateID) {
        return this.currentStates.contains(this.states[stateID]);
    }

    /**
     *
     * @param stateID
     * @return
     */
    public GadgetStates getState(int stateID) {
        for (GadgetStates state : currentStates)
            if (state == this.states[stateID])
                return state;
        return null;
    }
}
