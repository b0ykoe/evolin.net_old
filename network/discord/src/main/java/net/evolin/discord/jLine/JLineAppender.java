package net.evolin.discord.jLine;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.core.Layout;
import net.evolin.discord.Discord;

public final class JLineAppender extends AppenderBase<ILoggingEvent> {

    // Gets the custom layout from JLineLayout
    private final Layout<ILoggingEvent> layout = new JLineLayout();

    /**
     * starts the logger layout
     */
    @Override
    public void start() {
        super.start();
        layout.start();
    }

    /**
     * stops the logger layout
     */
    @Override
    public void stop() {
        layout.stop();
        super.stop();
    }

    /**
     * overrides the default logger and appends custom layout
     *
     * @param event event triggered on logging
     */
    @Override
    protected void append(ILoggingEvent event) {
        Discord.lineReader.printAbove(layout.doLayout(event));
    }
}