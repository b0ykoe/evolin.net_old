package net.evolin.papercore.gadgets.gadgets;

import net.evolin.papercore.PaperCore;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.entity.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.util.Vector;

import java.util.List;

@SuppressWarnings("FieldCanBeLocal")
public class BalloonGadget extends GadgetStates {

    private Sheep sheep;
    private ArmorStand armorStand;
    //ArmorStand moon;

    private PlayerObject playerObject;
    private Player player;
    private PaperCore paperCore;
    private int timer;

    @Override
    public void start(PaperCore paperCore, PlayerObject playerObject) {
        this.paperCore = paperCore;
        this.playerObject = playerObject;
        this.player = this.playerObject.getPlayer();
        this.player.setCollidable(false);

        PotionEffect potionEffect = new PotionEffect(PotionEffectType.INVISIBILITY, Integer.MAX_VALUE, Integer.MAX_VALUE, true, false);
        sheep = (Sheep) player.getWorld().spawnEntity(player.getLocation(), EntityType.SHEEP);
        armorStand = (ArmorStand) player.getWorld().spawnEntity(player.getLocation(), EntityType.ARMOR_STAND);
        //moon = (ArmorStand) player.getWorld().spawnEntity(player.getLocation(), EntityType.ARMOR_STAND);

        sheep.addPotionEffect(potionEffect);
        sheep.setLeashHolder(player);
        sheep.setSilent(true);
        sheep.setGravity(false);
        sheep.setCollidable(false);
        sheep.setVelocity(new Vector(0D, 0.4D, 0D));

        armorStand.setVisible(false);
        armorStand.setGravity(false);
        armorStand.setCanPickupItems(false);
        armorStand.setMarker(true);
        armorStand.setCollidable(false);

        this.paperCore.getUuidPlayerObject(player.getUniqueId()).change_visibility();
    }

    @Override
    public void stop() {
        sheep.remove();
        armorStand.remove();

        List<Entity> entList = player.getWorld().getEntities();

        for (Entity current : entList) {
            if (current instanceof Item)
                current.remove();
        }
    }

    /**
     * starts the ballon gadget and starts teleporting the "balloons" to the player
     */
    @Override
    public void run() {
        if (this.timer % 5 == 0) {
            if (this.sheep.getLocation().getY() > this.player.getLocation().add(0D, 2D, 0D).getY()) {
                this.sheep.setVelocity(new Vector(0D, -0.2D, 0D));
            } else {
                this.sheep.setVelocity(new Vector(0D, 0.4D, 0D));
            }
        }
        Vector origDirection = this.player.getLocation().getDirection();
        this.sheep.teleport(this.player.getLocation().add(getDirection(origDirection.multiply(-0.8D), 135F)).add(0D, 1.5D, 0D));
        this.armorStand.teleport(this.sheep.getLocation().add(0D, -0.3D, 0D).add(origDirection.multiply(-0.05D)));
        this.timer++;
    }

    /**
     * sets the item of the balloon
     *
     * @param item itemstack - fully builded itemstack
     */
    public void setItem(ItemStack item) {
        this.selectedItem = item;
        this.armorStand.setHelmet(item);
    }

    /**
     * gets the playerlooking direction
     *
     * @param vector    vector of the player
     * @param offsetYaw offset of the player
     * @return returns a vector to spawn the balloon
     */
    @SuppressWarnings("SameParameterValue")
    private Vector getDirection(Vector vector, Float offsetYaw) {
        double rotX = (double) player.getLocation().getYaw() + offsetYaw;
        double rotY = 0D; //(double)player.getLocation().getPitch();
        vector.setY(-Math.sin(Math.toRadians(rotY)));
        double xz = Math.cos(Math.toRadians(rotY));
        vector.setX(-xz * Math.sin(Math.toRadians(rotX)));
        vector.setZ(xz * Math.cos(Math.toRadians(rotX)));
        return vector;
    }

    /**
     * gets the sheep entity
     *
     * @return returns Sheep (instanceof Entity)
     */
    public Sheep getSheep() {
        return sheep;
    }

    /**
     * gets the armorstand of the ballon
     *
     * @return returns the armorstand
     */
    public ArmorStand getArmorStand() {
        return armorStand;
    }
}

