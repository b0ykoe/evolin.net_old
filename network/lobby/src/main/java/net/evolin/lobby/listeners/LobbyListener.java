package net.evolin.lobby.listeners;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.common.utils.ConfigUtils;
import net.evolin.common.utils.ServerUtils;
import net.evolin.lobby.Lobby;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

public class LobbyListener implements Listener {
    private Lobby lobby;

    public LobbyListener(Lobby lobby) {
        this.lobby = lobby;
    }

    @EventHandler
    public void handleJoin(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        ICloudPlayer cloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(player.getUniqueId());
        // if cloudPlayer exists
        if (cloudPlayer != null) {
            // if this is a forced swap load properties (usually not visible for the player
            if (cloudPlayer.getProperties().contains("forceSwap") || cloudPlayer.getProperties().contains("lobbySwap")) {
                if (cloudPlayer.getProperties().getBoolean("forceSwap"))
                    ServerUtils.loadSwapProperties(player);
                if (cloudPlayer.getProperties().getBoolean("lobbySwap"))
                    ServerUtils.loadSwapProperties(player);
                // if the player has last location on
            } else if (cloudPlayer.getProperties().contains("rememberLobbyLocation")) {
                if (cloudPlayer.getProperties().getBoolean("rememberLobbyLocation")) {
                    Location location = ConfigUtils.locationFromString(cloudPlayer.getProperties().getString("lastLobbyLocation"));
                    player.teleport(location);
                    // if nothing happens teleport to the default spawn
                } else {
                    player.teleport(ConfigUtils.locationFromString(this.lobby.paperCore.getConfigHandler().get("lobby", "spawnLocation", "lobby_thomas,0.5,101,0.5,-180,0")));
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void handleQuit(PlayerQuitEvent event) {
        Player player = event.getPlayer();
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());
        playerObject.getGadgetManager().stopCurrentStates();

        if(Boolean.parseBoolean(playerObject.getProperty("rememberLobbyLocation")))
            playerObject.addProperty(event.getPlayer().getUniqueId(), "lastLobbyLocation", ConfigUtils.locationToString(player.getLocation()));
    }

    @EventHandler
    public void handlePlayerMove(PlayerMoveEvent event) {
        Player player = event.getPlayer();
        if (!player.getGameMode().equals(GameMode.CREATIVE))
            if (player.getLocation().getY() <= Integer.parseInt(this.lobby.paperCore.getConfigHandler().get("lobby", "resetHeight", "60")))
                player.teleport(ConfigUtils.locationFromString(this.lobby.paperCore.getConfigHandler().get("lobby", "spawnLocation", "lobby_thomas,0.5,101,0.5,-180,0")));
    }
}
