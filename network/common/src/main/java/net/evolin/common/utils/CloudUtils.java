package net.evolin.common.utils;

import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.driver.service.ServiceTask;
import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.ServiceInfoSnapshotUtil;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;

public class CloudUtils {
    /**
     * counts players on a specific task
     *
     * @param taskName string - name of the task
     * @return int - playercount
     */
    public static int countServiceInfoSnapshotPlayerCount(String taskName) {
        int counter = 0;

        for (ServiceInfoSnapshot serviceInfoSnapshot : CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices(taskName)) {
            counter += ServiceInfoSnapshotUtil.getOnlineCount(serviceInfoSnapshot);
        }

        return counter;
    }

    /**
     * counts the amount of services online of a specific task
     *
     * @param taskName string - name of the task
     * @return - int serviceamount
     */
    public static int countServiceInfoSnapshotCount(String taskName) {
        return CloudNetDriver.getInstance().getCloudServiceProvider().getCloudServices(taskName).size();
    }

    /**
     * creates a service from a task and starts it
     *
     * @param taskName string - name of the task
     */
    public static void createStartSerivce(String taskName) {
        CloudNetDriver DRIVER = CloudNetDriver.getInstance();

        if (DRIVER.getServiceTaskProvider().isServiceTaskPresent(taskName)) {
            ServiceTask serviceTask = DRIVER.getServiceTaskProvider().getServiceTask(taskName); //getDef ServiceTask instance
            ServiceInfoSnapshot serviceInfoSnapshot = CloudNetDriver.getInstance().getCloudServiceFactory().createCloudService(serviceTask); //Creates a service on cluster and returns the initial snapshot

            DRIVER.getCloudServiceProvider(serviceInfoSnapshot).start(); //Starting service
        }
    }

    /**
     * adds a property to a cloudplayer
     *
     * @param iCloudPlayer icloudplayer - instance of the icloudplayer (BridgePlayerManager#getInstance()#getOnlinePlayer(uuid);
     * @param key          string - key of the property
     * @param value        string - value of the property
     */
    public static void setProperty(ICloudPlayer iCloudPlayer, String key, String value) {
        if (iCloudPlayer != null) {
            iCloudPlayer.getProperties()
                    .append(key, value);
            BridgePlayerManager.getInstance().updateOnlinePlayer(iCloudPlayer);
        }
    }

    /**
     * adds a property to a cloudplayer
     *
     * @param iCloudPlayer icloudplayer - instance of the icloudplayer (BridgePlayerManager#getInstance()#getOnlinePlayer(uuid);
     * @param key          string - key of the property
     * @param value        int - value of the property
     */
    public static void setProperty(ICloudPlayer iCloudPlayer, String key, int value) {
        setProperty(iCloudPlayer, key, String.valueOf(value));
    }

}
