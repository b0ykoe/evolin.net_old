package net.evolin.papercore.commands;

import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class SettingsCommand implements CommandExecutor {

    private PaperCore paperCore;

    public SettingsCommand(PaperCore paperCore) {
        this.paperCore = paperCore;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        Player player = (Player) sender;
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(player.getUniqueId());
        playerObject.openDefaultInventory("inventorySettings");
        return true;
    }
}
