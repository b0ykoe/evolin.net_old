package net.evolin.citybuild.listeners;

import net.evolin.citybuild.Citybuild;
import net.evolin.common.objects.EcoObject;
import net.evolin.papercore.handler.ScoreboardHandler;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import java.text.NumberFormat;
import java.util.Locale;

public class PlayerListener implements Listener {

    private Citybuild citybuild;

    public PlayerListener(Citybuild citybuild) {
        this.citybuild = citybuild;
    }

    /**
     * adds some functionality to the playerobject
     * @param event PlayerJoinEvent
     */
    @EventHandler(priority = EventPriority.HIGHEST)
    public void handle(PlayerJoinEvent event) {
        Player player = event.getPlayer();
        player.teleport(new Location(this.citybuild.getServer().getWorld("cbplotworld"), 230.5, 67, -189.5, 180, 0));
        player.setScoreboard(Bukkit.getScoreboardManager().getNewScoreboard());
        PlayerObject playerObject = this.citybuild.paperCore.getUuidPlayerObject(player.getUniqueId());
        Bukkit.getScheduler().runTaskLater(this.citybuild, () -> {
            playerObject.setScoreboardHandler(new ScoreboardHandler(playerObject, "CityBuild"));
            ScoreboardHandler scoreboardHandler = playerObject.getScoreboardHandler();

            NumberFormat formatter = NumberFormat.getInstance(new Locale("en_US"));

            scoreboardHandler.setLine(12, "§a");
            scoreboardHandler.setLine(11, "§7» Online:");
            scoreboardHandler.setLine(10, String.format(" §9%s/%d",  Bukkit.getOnlinePlayers().size(), 100));
            scoreboardHandler.setLine(9, "§b");
            scoreboardHandler.setLine(8, "§7» Dein Rang:");
            scoreboardHandler.setLine(7, String.format(" %s", ChatColor.translateAlternateColorCodes('&', playerObject.getiPermissionGroup().getPrefix())));
            scoreboardHandler.setLine(6, "§c");
            scoreboardHandler.setLine(5, "§7» Coins:");
            scoreboardHandler.setLine(4, String.format(" §6%s", formatter.format(EcoObject.getCoins(playerObject.getiCloudPlayer()))));
            scoreboardHandler.setLine(3, "§d");
            scoreboardHandler.setLine(2, "§7» Homepage:");
            scoreboardHandler.setLine(1, " §bClanCity.net");
            scoreboardHandler.setLine(0, "§e");

        }, 2);
        event.setJoinMessage("§8§l[§a§l+§8§l] §r" + ChatColor.translateAlternateColorCodes('&', playerObject.getPrefixDependsNick()) + "§7" +  playerObject.getCurrentProfile().getName());
    }

    @EventHandler
    public void handle(PlayerQuitEvent event) {
        PlayerObject playerObject = this.citybuild.paperCore.getUuidPlayerObject(event.getPlayer().getUniqueId());
        event.setQuitMessage("§8§l[§c§l-§8§l] §r" + ChatColor.translateAlternateColorCodes('&', playerObject.getPrefixDependsNick()) + "§7" + playerObject.getCurrentProfile().getName());
    }
}
