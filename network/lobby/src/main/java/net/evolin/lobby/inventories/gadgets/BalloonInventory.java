package net.evolin.lobby.inventories.gadgets;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.lobby.Lobby;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.gadgets.gadgets.BalloonGadget;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.inventories.InventoryUtils;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class BalloonInventory implements InventoryProvider {

    private Lobby lobby;
    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;
    private ItemHandler itemHandler;

    public BalloonInventory(Lobby lobby) {
        this.lobby = lobby;
        this.configHandler = this.lobby.paperCore.getConfigHandler();
        this.languageHandler = this.lobby.paperCore.getLanguageHandler();
        this.itemHandler = this.lobby.paperCore.getItemHandler();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());
        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.GadgetMenue(contents, playerObject);
        ItemStack item = null;
        if (playerObject.getGadgetManager().hasState(GadgetStates.BALLOONS)) {
            item = ((BalloonGadget) playerObject.getGadgetManager().getState(GadgetStates.BALLOONS)).getSelectedItem();
        }


        // set balloons -- Green, Blue, Orange, Yellow, Red
        contents.set(1, 2, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemBalloonGreen", playerObject) , event -> playerObject.setBalloon(playerObject.getDefaultItemObject("itemBalloonGreen").build(), "itemBalloonGreen")));
        contents.set(1, 3, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemBalloonBlue", playerObject) , event -> playerObject.setBalloon(playerObject.getDefaultItemObject("itemBalloonBlue").build(), "itemBalloonBlue")));
        contents.set(1, 4, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemBalloonOrange", playerObject) , event -> playerObject.setBalloon(playerObject.getDefaultItemObject("itemBalloonOrange").build(), "itemBalloonOrange")));
        contents.set(1, 5, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemBalloonYellow", playerObject) , event -> playerObject.setBalloon(playerObject.getDefaultItemObject("itemBalloonYellow").build(), "itemBalloonYellow")));
        contents.set(1, 6, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemBalloonRed", playerObject) , event -> playerObject.setBalloon(playerObject.getDefaultItemObject("itemBalloonRed").build(), "itemBalloonRed")));

    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
}
