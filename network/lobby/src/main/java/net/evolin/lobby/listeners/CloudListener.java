package net.evolin.lobby.listeners;

import de.dytanic.cloudnet.driver.event.EventListener;
import de.dytanic.cloudnet.driver.event.events.channel.ChannelMessageReceiveEvent;
import de.dytanic.cloudnet.ext.bridge.event.BridgeUpdateCloudPlayerEvent;
import net.evolin.common.utils.ServerUtils;
import net.evolin.lobby.Lobby;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import java.util.Map;

public class CloudListener implements Listener {

    private Lobby lobby;

    public CloudListener(Lobby lobby) {
        this.lobby = lobby;
    }

    /**
     * waits for lobby's to send confirmation on started
     * @param event ChannelMessageReceiveEvent from CloudNet
     */
    @EventListener
    public void handleChannelMessage(ChannelMessageReceiveEvent event) {
        if (event.getChannel().equalsIgnoreCase("serverStartChannel")) {
            if ("lobby".equalsIgnoreCase(event.getMessage()) && this.lobby.shutdown) {
                ServerUtils.safeSwapProperties(this.lobby.getServer().getOnlinePlayers(), event.getData().getString("serviceName"), "forceSwap");
            }
        }
        if(event.getChannel().equalsIgnoreCase("reloadInit")) {
            for (Map.Entry<String, Map<String, Map<String, String>>> entry : this.lobby.paperCore.getLanguageHandler().languages.entrySet()) {
                this.lobby.init(entry.getKey());
            }
        }
    }

    @EventListener
    public void handle(BridgeUpdateCloudPlayerEvent event) {
        Player player = this.lobby.getServer().getPlayer(event.getCloudPlayer().getUniqueId());
        if(player != null)
            if(this.lobby.getServer().getPlayer(event.getCloudPlayer().getUniqueId()) != null)
                if(event.getCloudPlayer().getProperties().contains("forceSwap"))
                    if(event.getCloudPlayer().getProperties().getBoolean("forceSwap")) {
                        ServerUtils.sendPlayerTo(player, event.getCloudPlayer().getProperties().getString("swapTo"));
                        if(!this.lobby.shutdownTimer) {
                            this.lobby.shutdownTimer = true;
                            this.lobby.getServer().getScheduler().runTaskTimerAsynchronously(this.lobby, () -> ServerUtils.shutdownServerIfNull(lobby), 40, 20);
                        }
                    }
    }
}
