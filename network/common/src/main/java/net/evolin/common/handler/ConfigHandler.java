package net.evolin.common.handler;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;

import java.io.Serializable;
import java.util.Map;

public class ConfigHandler implements Serializable {

    public Map<String, Map<String, String>> config;

    /**
     * returns value out of the config
     *
     * @param authority authority (example general, bedwars and so on)
     * @param key       key in the authority
     * @return string with a default value or config value
     */
    public String get(String authority, String key, String defaultValue) {
        // if authority (example general, bedwars and so on) does not exist ->  return default value
        if (!config.containsKey(authority)) {
            System.out.println("ConfigHandler: {%authority%}/{%key%} authority and key not found".replace("%authority%", authority).replace("%key%", key));
            callback(authority, key, defaultValue);
            return defaultValue;
        }

        // if the authority exists but the key does not (example general exists but prefix does not) -> return default value
        if (!config.get(authority).containsKey(key)) {
            System.out.println("ConfigHandler: {%authority%}/{%key%} key not found".replace("%authority%", authority).replace("%key%", key));
            callback(authority, key, defaultValue);
            return defaultValue;
        }

        // else return whatever is in the config
        return config.get(authority).get(key);
    }

    private void callback(String authority, String key, String defaultValue) {
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("missingConfig", "" , new JsonDocument()
                .append("authority", authority)
                .append("key", key)
                .append("value", defaultValue));
    }
}
