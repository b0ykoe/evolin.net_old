package net.evolin.lobby;

public class Config {


    /**
     * PERMISSIONS
     */

    public static String PERM_nick = "core.nickSystem";
    public static String PERM_silentLobby = "core.silentLobby";
    public static String PERM_vip = "core.vip";
    public static String PERM_doublejump = "core.doublejump";

    // inventory Titles
    public static String TITLE_Shoes = "Schuhe";
    public static String TITLE_Hide = "Hide Inventar";
    public static String TITLE_Compass = "Compass";
    public static String TITLE_Gadgets = "Gadgets";
    public static String TITLE_Friends = "Freunde";
    public static String TITLE_Settings = "Einstellungen";
    public static String TITLE_Particles = "Partikles";
    public static String TITLE_Balloons = "Luftballons";
    public static String TITLE_Scoreboard = "§f§lLITGAMES.EU";

}
