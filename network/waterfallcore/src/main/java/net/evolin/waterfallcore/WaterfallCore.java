package net.evolin.waterfallcore;

import de.dytanic.cloudnet.common.document.gson.JsonDocument;
import de.dytanic.cloudnet.driver.CloudNetDriver;
import de.dytanic.cloudnet.wrapper.Wrapper;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.waterfallcore.commands.EcoCommand;
import net.evolin.waterfallcore.commands.MoneyCommand;
import net.evolin.waterfallcore.commands.eco.PayCommand;
import net.evolin.waterfallcore.listeners.ConfigListener;
import net.evolin.common.objects.EcoObject;
import net.md_5.bungee.api.plugin.Plugin;

import java.util.UUID;

public final class WaterfallCore extends Plugin {

    private UUID serviceId = Wrapper.getInstance().getServiceId().getUniqueId();
    public LanguageHandler languageHandler;
    public ConfigHandler configHandler;
    public EcoObject ecoObject;

    @Override
    public void onEnable() {
        // Plugin startup logic
        getProxy().getPluginManager().registerCommand(this, new EcoCommand(this,"eco", "", ""));
        getProxy().getPluginManager().registerCommand(this, new MoneyCommand("money"));
        getProxy().getPluginManager().registerCommand(this, new PayCommand("pay"));


        // register for CloudNet Events
        CloudNetDriver.getInstance().getEventManager().registerListener(new ConfigListener(this));

        // calls a sendChannelMessage to request the current configuration after start
        CloudNetDriver.getInstance().getMessenger().sendChannelMessage("requestConfig", String.valueOf(this.serviceId), new JsonDocument());
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        CloudNetDriver.getInstance().getEventManager().unregisterListeners(this.getClass().getClassLoader()); //Removes all Event listeners from this plugin
        Wrapper.getInstance().getServicesRegistry().unregisterAll(this.getClass().getClassLoader()); //Removes all ServiceRegistry items if they exists
        Wrapper.getInstance().unregisterPacketListenersByClassLoader(this.getClass().getClassLoader()); //Removes all IPacketListener implementations on all channels from the network connector
    }
}
