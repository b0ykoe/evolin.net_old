package net.evolin.lobby.inventories.gadgets;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.lobby.Lobby;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.gadgets.gadgets.ParticleGadget;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.inventories.InventoryUtils;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class ParticleInventory implements InventoryProvider {

    private Lobby lobby;
    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;
    private ItemHandler itemHandler;

    public ParticleInventory(Lobby lobby) {
        this.lobby = lobby;
        this.configHandler = this.lobby.paperCore.getConfigHandler();
        this.languageHandler = this.lobby.paperCore.getLanguageHandler();
        this.itemHandler = this.lobby.paperCore.getItemHandler();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());
        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.GadgetMenue(contents, playerObject);

        ItemStack item = null;
        if (playerObject.getGadgetManager().hasState(GadgetStates.PARTICLES)) {
            item = ((ParticleGadget) playerObject.getGadgetManager().getState(GadgetStates.PARTICLES)).getSelectedItem();
        }

        contents.set(1, 2, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemBlazeParticles", playerObject), event -> setParticle(playerObject, player, "itemBlazeParticles", Particle.FLAME) ));
        contents.set(1, 4, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemLapisParticles", playerObject), event -> setParticle(playerObject, player, "itemLapisParticles", Particle.WATER_SPLASH) ));
        contents.set(1, 6, ClickableItem.of(InventoryUtils.checkCurrentItem(item, "itemEnderParticles", playerObject), event -> setParticle(playerObject, player, "itemEnderParticles", Particle.PORTAL) ));
    }

    public static void setParticle(PlayerObject playerObject, Player player, String item, Particle particle) {
        playerObject.addProperty("currentParticlesItem", item);
        playerObject.addProperty("currentParticlesParticles", particle.toString());
        playerObject.getGadgetManager().setState(GadgetStates.PARTICLES);
        if(playerObject.getGadgetManager().hasState(GadgetStates.PARTICLES)) {
            ((ParticleGadget) playerObject.getGadgetManager().getState(GadgetStates.PARTICLES)).setParticle(playerObject.getDefaultItemObject(item).build(), particle);
            player.closeInventory();
            player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
        }
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
}
