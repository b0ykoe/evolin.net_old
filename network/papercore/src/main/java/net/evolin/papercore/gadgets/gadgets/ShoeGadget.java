package net.evolin.papercore.gadgets.gadgets;

import net.evolin.papercore.PaperCore;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;


public class ShoeGadget extends GadgetStates {

    private PaperCore paperCore;
    private PlayerObject playerObject;
    private ItemHandler itemHandler;

    @Override
    public void start(PaperCore paperCore, PlayerObject playerObject) {
        this.paperCore = paperCore;
        this.playerObject = playerObject;
        this.itemHandler = this.paperCore.getItemHandler();
    }

    /**
     *
     * @param shoes
     */
    public void setShoes(ItemStack shoes) {
        Player player = this.playerObject.getPlayer();
        this.selectedItem = shoes;
        player.getInventory().setBoots(shoes);
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
        if (this.playerObject.getDefaultItemObject("itemAquaShoes").build().equals(shoes)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, Integer.MAX_VALUE, 3, false, false));
        } else if (this.playerObject.getDefaultItemObject("itemGrayShoes").build().equals(shoes)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, Integer.MAX_VALUE, 4, false, false));
        } else if (this.playerObject.getDefaultItemObject("itemOrangeShoes").build().equals(shoes)) {
            player.addPotionEffect(new PotionEffect(PotionEffectType.JUMP, Integer.MAX_VALUE, 3, false, false));
        }
    }

    @Override
    public void stop() {
        Player player = this.playerObject.getPlayer();
        player.getInventory().setBoots(new ItemStack(Material.AIR));
        player.getActivePotionEffects().forEach(potionEffect -> player.removePotionEffect(potionEffect.getType()));
    }
}

