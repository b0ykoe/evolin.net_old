package net.evolin.waterfallcore.commands.eco;

import de.dytanic.cloudnet.ext.bridge.BridgePlayerManager;
import de.dytanic.cloudnet.ext.bridge.player.ICloudPlayer;
import net.evolin.common.utils.SystemUtils;
import net.evolin.waterfallcore.WaterfallCore;
import net.evolin.waterfallcore.commands.SubCommand;
import net.evolin.common.objects.EcoObject;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;

public class EcoSet extends SubCommand {
    public EcoSet(WaterfallCore waterfallCore) {
        super(waterfallCore, "set", "system.eco.set","set [name] <amount>", "Setzt eine Menge an Money");
    }

    @Override
    public void execute(ProxiedPlayer player, String[] args) {
        ProxiedPlayer receiver = null;
        ICloudPlayer iCloudPlayer = null;
        int amount = 0;

        if(!this.hasPermission(player))
            return;

        if(args.length == 0)
            return;

        if(args.length == 1) {
            receiver = player;
            amount = SystemUtils.parseWithDefault(args[0], 0);
        }

        if(args.length == 2) {
            receiver = ProxyServer.getInstance().getPlayer(args[0]);
            amount = SystemUtils.parseWithDefault(args[1], 0);
        }

        if(receiver == null) {
            player.sendMessage("Der Spieler %s ist nicht online".replace("%s", args[0]));
            return;
        }

        iCloudPlayer = BridgePlayerManager.getInstance().getOnlinePlayer(receiver.getUniqueId());

        EcoObject.setCoins(iCloudPlayer, amount);
        if(!iCloudPlayer.getName().equals(player.getName())) {
            player.sendMessage("Du hast die Coins von dem Spieler %s auf %i gesetzt".replace("%s", iCloudPlayer.getName()).replace("%i", String.valueOf(amount)));
            BridgePlayerManager.getInstance().proxySendPlayerMessage(iCloudPlayer, "Deine Eco wurde auf %i gesetzt".replace("%i", String.valueOf(amount)));
        } else
            player.sendMessage("Du hast deine Coins auf %i gesetzt".replace("%i", String.valueOf(amount)));
    }
}
