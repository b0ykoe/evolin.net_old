package net.evolin.papercore.handler;

import net.evolin.common.objects.EcoObject;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.*;

import java.text.NumberFormat;
import java.util.Locale;

public class ScoreboardHandler {
    private PlayerObject playerObject;
    private String scoreboardTitle;
    private Scoreboard scoreboard;

    public ScoreboardHandler(PlayerObject playerObject, String scoreboardTitle) {
        this.playerObject = playerObject;
        this.scoreboardTitle = scoreboardTitle;
        init();
    }

    private void init(){
        if(this.playerObject == null)
            return;

        Player player = this.playerObject.getPlayer();
        this.scoreboard = this.playerObject.getPlayer().getScoreboard();

        if(scoreboard.getObjective(this.scoreboardTitle) == null)
            scoreboard.registerNewObjective(this.scoreboardTitle, "dummy", this.scoreboardTitle);

        Objective objective = player.getScoreboard().getObjective(this.scoreboardTitle);
        objective.setDisplaySlot(DisplaySlot.SIDEBAR);

        player.setScoreboard(scoreboard);
    }

    public void setLine(int id, String prefix) {
        String t = prefix;
        Objective obj = this.scoreboard.getObjective(DisplaySlot.SIDEBAR);
        Score score = obj.getScore(ChatColor.values()[id] + "");
        score.setScore(id);
        if(this.scoreboard.getTeam(ChatColor.values()[id] + "") == null)
            this.scoreboard.registerNewTeam(ChatColor.values()[id] + "");

        Team team = scoreboard.getTeam(ChatColor.values()[id] + "");
        if(!team.hasEntry(ChatColor.values()[id] + ""))
            team.addEntry(ChatColor.values()[id] + "");

        if (t.length() <= 12) {
            team.setPrefix(t);
            return;
        }

        // If the text actually goes above 32, cut it to 32 to prevent kicks and errors
        if (t.length() > 32) {
            t = t.substring(0,32);
        }

        String test = t.replace(" ", "");
        String colorcode = (test.startsWith("§")) ? test.substring(0, 2) : "";

        // Set the prefix to the first 12 characters
        team.setPrefix(t.substring(0, 12));

        // Now use the last 20 characters and put them into the suffix
        team.setSuffix(colorcode + t.substring(12));

    }

    public void updateLine(int id, String text) {
        Team team = this.playerObject.getPlayer().getScoreboard().getTeam(ChatColor.values()[id] + "");
        team.setPrefix(text);
    }

    public void update() {
        NumberFormat formatter = NumberFormat.getInstance(new Locale("en_US"));

        this.updateLine(4, String.format(" §6%s", formatter.format(EcoObject.getCoins(playerObject.getiCloudPlayer()))));
        this.updateLine(7, String.format(" §r%s", ChatColor.translateAlternateColorCodes('&', playerObject.getiPermissionGroup().getPrefix())));
        this.updateLine(10, String.format(" §9%s/%d", Bukkit.getOnlinePlayers().size(), 100));
    }
}


