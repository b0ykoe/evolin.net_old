package net.evolin.papercore.inventories;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.entity.Player;

public class LobbySettingsInventory implements InventoryProvider {

    private PaperCore paperCore;
    private LanguageHandler languageHandler;
    public LobbySettingsInventory(PaperCore paperCore) {
        this.paperCore = paperCore;
        this.languageHandler = this.paperCore.getLanguageHandler();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(player.getUniqueId());
        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.SettingsMenuSub(contents, playerObject, this.languageHandler);
        InventoryUtils.ProfileMenu(contents, playerObject, this.languageHandler);

        contents.set(1, 4, ClickableItem.of(playerObject.getDefaultItemObject("itemLobbySettingsSafePosition").build(), e -> { }));

        if(!Boolean.parseBoolean(playerObject.getProperty("rememberLobbyLocation")))
            contents.set(2, 4, ClickableItem.of(playerObject.getDefaultItemObject("itemOff").build(), e -> {
                playerObject.addProperty("rememberLobbyLocation", "true");
                playerObject.sendMessageToPlayer(playerObject.getDefaultLang(this.languageHandler, "lobby-settings-rememberLocation-on", "You'll respawn at the same position after reconnecting."));
                playerObject.openDefaultInventory("inventoryLobbySettings");
            }));
        else
            contents.set(2, 4, ClickableItem.of(playerObject.getDefaultItemObject("itemOn").build(), e -> {
                playerObject.addProperty("rememberLobbyLocation", "false");
                playerObject.sendMessageToPlayer(playerObject.getDefaultLang(this.languageHandler, "lobby-settings-rememberLocation-off", "You'll respawn at the default position upon reconnecting."));
                playerObject.openDefaultInventory("inventoryLobbySettings");
            }));
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }

}
