package net.evolin.papercore.inventories;

import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.entity.Player;

public class SettingsInventory implements InventoryProvider {

    private PaperCore paperCore;
    private LanguageHandler languageHandler;
    public SettingsInventory(PaperCore paperCore) {
        this.paperCore = paperCore;
        this.languageHandler = this.paperCore.getLanguageHandler();
    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.paperCore.getUuidPlayerObject(player.getUniqueId());
        InventoryUtils.BasicInventoryLayout(contents, playerObject);
        InventoryUtils.SettingsMenuMain(contents, playerObject, this.languageHandler);
        InventoryUtils.ProfileMenu(contents, playerObject, this.languageHandler);
    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
}
