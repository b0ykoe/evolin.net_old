package net.evolin.papercore.inventories;

import de.dytanic.cloudnet.driver.service.ServiceId;
import de.dytanic.cloudnet.driver.service.ServiceInfoSnapshot;
import de.dytanic.cloudnet.ext.bridge.ServiceInfoSnapshotUtil;
import de.dytanic.cloudnet.wrapper.Wrapper;
import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.common.utils.ServerUtils;
import net.evolin.papercore.PaperCore;
import net.evolin.papercore.gadgets.GadgetStates;
import net.evolin.papercore.objects.ItemObject;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.Collection;
import java.util.Comparator;

public class InventoryUtils {

    public static InventoryContents BasicInventoryLayout (InventoryContents contents, PlayerObject playerObject) {
        for (int fillRow = 0; fillRow < 9; fillRow++) {
            contents.set(3 , fillRow, ClickableItem.empty(playerObject.getDefaultItemObject("emptyItem").build()));
            if(fillRow != 0 || fillRow != 8)
                contents.set(1 , fillRow, ClickableItem.empty(playerObject.getDefaultItemObject("emptyItem").build()));
        }

        return contents;
    }

    public static void TeleporterMenue(InventoryContents contents, PlayerObject playerObject) {
        if(playerObject.getPlayer().hasPermission("network.access.silent-lobby")) {
            contents.set(4, 4, ClickableItem.of(playerObject.getDefaultItemObject("itemSilentLobbySwitcher").build(), e -> {
                playerObject.getDefaultInventory("inventorySilentLobbySwitcher").open(playerObject.getPlayer());
            }));
        }
        contents.set(4, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemLobbySwitcher").build(), e -> {
            playerObject.getDefaultInventory("inventoryLobbySwitcher").open(playerObject.getPlayer());
        }));
        contents.set(4, 6, ClickableItem.of(playerObject.getDefaultItemObject("itemTeleporter").build(), e -> {
            playerObject.getDefaultInventory("inventoryTeleporter").open(playerObject.getPlayer());
        }));
    }

    public static void GadgetMenue(InventoryContents contents, PlayerObject playerObject) {
        contents.set(4, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemParticles").build(), e -> {
            playerObject.getDefaultInventory("inventoryParticles").open(playerObject.getPlayer());
        }));

        contents.set(4, 4, ClickableItem.of(playerObject.getDefaultItemObject("itemBalloons").build(), e -> {
            playerObject.getDefaultInventory("inventoryBalloons").open(playerObject.getPlayer());
        }));
        contents.set(4, 6, ClickableItem.of(playerObject.getDefaultItemObject("itemShoes").build(), e -> {
            playerObject.getDefaultInventory("inventoryShoes").open(playerObject.getPlayer());
        }));

        // remove -
        contents.set(3, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemRemoveParticles").build(), e -> {
            playerObject.getGadgetManager().stopCurrentState(GadgetStates.PARTICLES);
            playerObject.addProperty("currentParticlesItem", "none");
            playerObject.addProperty("currentParticlesParticles", "none");
            playerObject.getPlayer().closeInventory();
            playerObject.getPlayer().playSound(playerObject.getPlayer().getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
        }));

        contents.set(3, 4, ClickableItem.of(playerObject.getDefaultItemObject("itemRemoveBalloons").build(), e -> {
            playerObject.getGadgetManager().stopCurrentState(GadgetStates.BALLOONS);
            playerObject.addProperty("currentBalloon", "none");
            playerObject.getPlayer().closeInventory();
            playerObject.getPlayer().playSound(playerObject.getPlayer().getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
        }));

        contents.set(3, 6, ClickableItem.of(playerObject.getDefaultItemObject("itemRemoveShoes").build(), e -> {
            playerObject.getGadgetManager().stopCurrentState(GadgetStates.SHOES);
            playerObject.addProperty("currentShoes", "none");
            playerObject.getPlayer().closeInventory();
            playerObject.getPlayer().playSound(playerObject.getPlayer().getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
        }));

    }



    public static void CurrentServerItem(InventoryContents contents, PlayerObject playerObject, LanguageHandler languageHandler) {
        ServiceId currServer = Wrapper.getInstance().getServiceId();
        ServiceInfoSnapshot currServerInfo = Wrapper.getInstance().getCurrentServiceInfoSnapshot();
        contents.set(3, 4, ClickableItem.of(
                playerObject.getDefaultItemObject("currentServer")
                        .setDisplayName("§a" + currServer.getName())
                        .setLore( playerObject.getDefaultLang(languageHandler, "currentServerItem", "§cYou're here."),
                                "§c" + ServiceInfoSnapshotUtil.getOnlineCount(currServerInfo) + " / " + ServiceInfoSnapshotUtil.getMaxPlayers(currServerInfo))
                        .build(), e -> {
                }));
    }

    public static void ServerList(Player player, InventoryContents contents, PlayerObject playerObject, int[] row, Collection<ServiceInfoSnapshot> cloudServices, PaperCore paperCore) {
        InventoryContents finalContents = contents;
        cloudServices.stream()
                .sorted(InventoryUtils.CloudNetComperator())
                .filter(v -> !(paperCore.getServiceId().getName().equals(v.getServiceId().getName())))
                .forEach(serviceInfoSnapshot -> {
                    if(row[0] == 9)
                        return;
                    finalContents.set(1, row[0],  ClickableItem.of(
                            playerObject.getDefaultItemObject("onlineServer")
                                    .setDisplayName("§a" + serviceInfoSnapshot.getServiceId().getName())
                                    .setLore("§c" + ServiceInfoSnapshotUtil.getOnlineCount(serviceInfoSnapshot) + " / " + ServiceInfoSnapshotUtil.getMaxPlayers(serviceInfoSnapshot))
                                    .build().clone(), e -> {
                                ServerUtils.safeSwapPropertie(player, serviceInfoSnapshot.getServiceId().getName(), "lobbySwap");
                                ServerUtils.sendPlayerTo(player, serviceInfoSnapshot.getServiceId().getName());
                            }));
                    row[0]++;
                });
    }

    public static void ProfileMenu(InventoryContents contents, PlayerObject playerObject, LanguageHandler languageHandler) {
        contents.set(4, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemSettings").build(), e -> {
            playerObject.getDefaultInventory("inventorySettings").open(playerObject.getPlayer());

        }));

        ItemStack head = new ItemObject(playerObject.getPlayerHead()).setDisplayName(playerObject.getDefaultLang(languageHandler, "itemProfileTitle", "§cProfile")).build().clone();
        contents.set(4, 4, ClickableItem.of(head, e -> {
            playerObject.getDefaultInventory("inventoryProfile").open(playerObject.getPlayer());

        }));
    }

    public static void SettingsMenuMain(InventoryContents contents, PlayerObject playerObject, LanguageHandler languageHandler) {
        contents.set(1, 6, ClickableItem.of(playerObject.getDefaultItemObject("itemLanguage").build(), e -> {
            playerObject.getDefaultInventory("inventoryLanguage").open(playerObject.getPlayer());
        }));
        contents.set(1, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemLobbySettings").build(), e -> {
            playerObject.getDefaultInventory("inventoryLobbySettings").open(playerObject.getPlayer());
        }));

    }

    public static void SettingsMenuSub(InventoryContents contents, PlayerObject playerObject, LanguageHandler languageHandler) {
        contents.set(3, 6, ClickableItem.of(playerObject.getDefaultItemObject("itemLanguage").build(), e -> {
            playerObject.getDefaultInventory("inventoryLanguage").open(playerObject.getPlayer());
        }));
        contents.set(3, 2, ClickableItem.of(playerObject.getDefaultItemObject("itemLobbySettings").build(), e -> {
            playerObject.getDefaultInventory("inventoryLobbySettings").open(playerObject.getPlayer());
        }));
    }

    public static Comparator<ServiceInfoSnapshot> CloudNetComperator() {
        return (i1, i2) -> i2.getServiceId().getName().compareTo(i1.getServiceId().getName());
    }

    public static ItemStack checkCurrentItem(ItemStack itemStack, String item, PlayerObject playerObject) {
        if(itemStack == null)
            return playerObject.getDefaultItemObject(item).build();

        if(itemStack.equals(playerObject.getDefaultItemObject(item).build()))
            return new ItemObject(playerObject.getDefaultItemObject(item).build().clone()).hideEnchantments().clone();

        return playerObject.getDefaultItemObject(item).build();
    }
}
