package net.evolin.module.sockets;

import net.evolin.common.simpleServerClient.Datapackage;

public class Client extends net.evolin.common.simpleServerClient.Client{

    public Client(String hostname, int port, String id) {
        super(hostname, port, id);

        // auto start the client
        //start();
    }

    public synchronized Client getClient(){
        return this;
    }

    public synchronized void sendCommand(String command) {
        this.sendMessage(new Datapackage("command", command));
    }
}
