package net.evolin.lobby.inventories;

import fr.minuskube.inv.ClickableItem;
import fr.minuskube.inv.content.InventoryContents;
import fr.minuskube.inv.content.InventoryProvider;
import net.evolin.common.handler.ConfigHandler;
import net.evolin.common.handler.LanguageHandler;
import net.evolin.lobby.Lobby;
import net.evolin.papercore.handler.ItemHandler;
import net.evolin.papercore.objects.PlayerObject;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class HideInventory implements InventoryProvider {

    private Lobby lobby;
    private ConfigHandler configHandler;
    private LanguageHandler languageHandler;
    private ItemHandler itemHandler;

    public HideInventory(Lobby lobby) {
        this.lobby = lobby;
        this.configHandler = this.lobby.paperCore.getConfigHandler();
        this.languageHandler = this.lobby.paperCore.getLanguageHandler();
        this.itemHandler = this.lobby.paperCore.getItemHandler();    }

    @Override
    public void init(Player player, InventoryContents contents) {
        PlayerObject playerObject = this.lobby.paperCore.getUuidPlayerObject(player.getUniqueId());

        contents.fillColumn(0, ClickableItem.of(playerObject.getVisibility() == PlayerObject.VISIBILITY.ALL ? playerObject.getDefaultItemObject("ITEM_green_all_enchanted").build() :  playerObject.getDefaultItemObject("ITEM_green_all").build(), e -> {
            if(e.isLeftClick() || e.isRightClick())
            {
                playerObject.addProperty("currentVisibility", PlayerObject.VISIBILITY.ALL.toString());
                playerObject.setVisibility(PlayerObject.VISIBILITY.ALL);
                playerObject.change_visibility();
                player.closeInventory();
                player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                player.sendActionBar(playerObject.getDefaultLang(this.languageHandler, "ACTION_BAR_EVERYONE", "§cou may now see all players."));
            }
        }));
        contents.fillColumn(1, ClickableItem.of(playerObject.getVisibility() == PlayerObject.VISIBILITY.VIP ? playerObject.getDefaultItemObject("ITEM_lila_vip_enchanted").build() :  playerObject.getDefaultItemObject("ITEM_lila_vip").build(), e -> {
            if(e.isLeftClick() || e.isRightClick())
            {
                playerObject.addProperty("currentVisibility", PlayerObject.VISIBILITY.VIP.toString());
                playerObject.setVisibility(PlayerObject.VISIBILITY.VIP);
                playerObject.change_visibility();
                player.closeInventory();
                player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                player.sendActionBar(playerObject.getDefaultLang(this.languageHandler, "ACTION_BAR_VIP", "§cYou can only see team members and youtubers now."));
            }
        }));
        contents.fillColumn(2, ClickableItem.of(playerObject.getVisibility() == PlayerObject.VISIBILITY.NONE ? playerObject.getDefaultItemObject("ITEM_gray_none_enchanted").build() :  playerObject.getDefaultItemObject("ITEM_gray_none").build(), e -> {
            if(e.isLeftClick() || e.isRightClick())
            {
                playerObject.addProperty("currentVisibility", PlayerObject.VISIBILITY.NONE.toString());
                playerObject.setVisibility(PlayerObject.VISIBILITY.NONE);
                playerObject.change_visibility();
                player.closeInventory();
                player.playSound(player.getLocation(), Sound.ENTITY_CHICKEN_EGG, 1, 1);
                player.sendActionBar(playerObject.getDefaultLang(this.languageHandler, "ACTION_BAR_NONE", "§cYou won't see any players now."));
            }
        }));

    }

    @Override
    public void update(Player player, InventoryContents contents) {

    }
}
